

   ##                SISTEMA DE CONTROLE DA SEMANA DE PÓS-GRADUAÇÃO NA BIBLIOTECA DA EESC
                   

Sistema de Controle da Semana de Pós-graduação na Biblioteca da EESC, que consiste em um  cadastro dos eventos, cursos, palestras, visitas, etc.
e tambem cadastro dos participantes da USP e externos tambem.
Inclusive com emissão de certificados de participação.

## INSTALAÇÃO


1.  Clonar repositório.
2.  No diretório 'config' renomear o arquivo config.sample.php para config.php e alterar as credenciais de acesso ao banco de dados.
3.  Configurar o apache apontando para a pasta www/
5.  Entrar no Servidor de Banco de Dados do MySql, e verificar a existencia do banco de dados eesc_biblioteca e as seguintes tabelas desse sistema: 
                        contato,
                        contato_n
                        downloads,
                        downloads_conteudo_n
                        downloads_n,
                        home,
                        home_n,
                        infos,
                        infos_n,
                        inscr_campanha_n,
                        inscr_conteudo_n,
                        inscricoes,
                        inscricoes_n,
                        links_n,
                        prog,
                        prog_n,
                        prog_n2021,
                        programacao_n,
                        sugestoes.
6. Gerar as que nao existirem utilizando os respectivos .sql
7. O acesso a esse módulo do sistema é público para inscrições.
8. Para atualização de determinado ano do evento é preciso atualizar os dados através de acesso ao aplicativo de administração desse sistema  da Semana da Pós-graduação.                       


Utilização

Acesso público, www.sistemas.eesc.usp.br/semana_pos2021

