<?php

$id_prog = $_GET["id_prog"];

error_reporting(E_ALL);
require_once("application_top.php");
error_reporting(E_ALL);
$tpl = new Template(".");
$tpl->set_file("main","sinopse.tpl");

$query = new dbSqlConnectionBIBLIOTECA();
$query->query("SELECT * FROM semanapos_prog_n2021 WHERE id_prog = $id_prog");
$query->next_record();

$semana = array("domingo","segunda-feira", "ter&ccedil;a-feira", "quarta-feira", "quinta-feira", "sexta-feira", "s&aacute;bado");

list($ano,$mes,$dia) = sscanf($query->f('data'),"%04d-%02d-%02d");
$data = sprintf("%02d/%02d/%04d",$dia,$mes,$ano);
$timestamp = mktime(0,0,0,$mes,$dia,$ano);
$tpl->set_var("DATA","$data - ".$semana[date("w",$timestamp)]);

$tpl->set_var("HORAINICIO",substr($query->f('horainicio'),0,2)."h".substr($query->f('horainicio'),3,2));
$tpl->set_var("HORAFIM", substr($query->f('horafim'),0,2)."h".substr($query->f('horafim'),3,2));
$tpl->set_var("TITULO", $query->f('titulo'));
$tpl->set_var("PALESTRANTE", $query->f('palestrante'));
$tpl->set_var("RESUMO", $query->f('resumo'));
$tpl->set_var("LOCAL", $query->f('local'));

$tpl->parse("final","main");
$tpl->p("final");

?>
