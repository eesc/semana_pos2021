#!/bin/bash
#  Script para conversão de encoding

TO=UTF-8
DIR=/home/eesc/public_html/administracao/biblioteca/pub/semana_pos2017

find $DIR -type f | while read i;

do

   ARQ=`echo $i`
   TPARQ=`file -i ${i} | awk {'print $3'} | tr ":" " " | sed 's/charset\=//g'`

   if [ $TPARQ != utf-8 -a $TPARQ != us-ascii ]; then

       FROM=$TPARQ
       ICONV="iconv -f $FROM -t $TO"
       
       echo -n "Alterando arquivo: " $ARQ " Tipo: " $TPARQ
       cp $ARQ $ARQ.bak
       $ICONV $ARQ.bak -o $ARQ
       rm $ARQ.bak
       echo

   else

      echo -n " Ignorando arquivo: " $ARQ " Tipo: " $TPARQ
      echo

   fi

done

