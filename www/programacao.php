<?php

require_once("application_top.php");

$tpl = new Template(".");
$tpl->set_file("main","programacao.tpl");
$tpl->set_block("main","block_DIA","bDIA");
$tpl->set_block("block_DIA","block_PROGS","bPROGS");

// SORTEIO DO APOIO :: ver application_top.php
//$tpl->set_var("BANNER",$virus);
//$tpl->set_var("A1","<img src=\"images/incentivo/apoio/$a1\" border=\"0\" height=\"85\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a2\" border=\"0\" height=\"85\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a3\" border=\"0\" height=\"85\" >");
//$tpl->set_var("A1","<img src=\"images/incentivo/apoio/$a1\" border=\"0\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a2\" border=\"0\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a3\" border=\"0\" >
//        &nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a4\" border=\"0\">&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a5\" border=\"0\" >");
$tpl->set_var("A1","<img src=\"images/incentivo/apoio/$a1\" border=\"0\" height=\"60\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a2\" border=\"0\" height=\"60\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a3\" border=\"0\" height=\"60\" >
        &nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a4\" border=\"0\" height=\"60\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a5\" border=\"0\" height=\"60\" >
");



$query = new dbSqlConnectionBIBLIOTECA();
$query->query("SELECT pagina FROM semanapos_programacao_n WHERE ano = 2021");

$query->next_record();
$tpl->set_var("PAGINA",$query->f('pagina'));

$query = new dbSqlConnectionBIBLIOTECA();
$query->query("SELECT DISTINCT(data) FROM semanapos_prog_n2021 ORDER BY data ASC");

$semana = array("domingo","segunda-feira", "ter&ccedil;a-feira", "quarta-feira", "quinta-feira", "sexta-feira", "s&aacute;bado");
while ($query->next_record())
{
	list($ano,$mes,$dia) = sscanf($query->f('data'),"%04d-%02d-%02d");
	$data = sprintf("%02d/%02d/%04d",$dia,$mes,$ano);
	$timestamp = mktime(0,0,0,$mes,$dia,$ano);
	$tpl->set_var("DIA","$data - ".$semana[date("w",$timestamp)]);

	$aux = new dbSqlConnectionBIBLIOTECA();
	$aux->query("SELECT * FROM semanapos_prog_n2021 WHERE data = '".$query->f('data')."' ORDER BY horainicio, horafim");
	while($aux->next_record())
	{
		$classe = $classe == "claro" ? "escuro" : "claro";
		$tpl->set_var("HORAINICIO",substr($aux->f('horainicio'),0,2)."h".substr($aux->f('horainicio'),3,2));
		$tpl->set_var("HORAFIM", substr($aux->f('horafim'),0,2)."h".substr($aux->f('horafim'),3,2));
		$tpl->set_var("CLASSE",$classe);
		$tpl->set_var("ID_PROG",$aux->f('id_prog'));
		$tpl->set_var("TITULO", $aux->f('titulo'));
		$tpl->set_var("PALESTRANTE", $aux->f('palestrante'));
		$tpl->set_var("LOCAL", $aux->f('local'));
		$tpl->parse("bPROGS","block_PROGS",true);
	}
	$tpl->parse("bDIA","block_DIA",true);
	$tpl->set_var("bPROGS", null);
}

$tpl->parse("final","main");
$tpl->p("final");

?>
