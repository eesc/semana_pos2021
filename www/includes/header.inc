<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

 <head>
  <title>XVII Semana da Pós-Graduação na EESC e II Semana da Pós-Graduação do ICMC</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta http-equiv="Content-Language" content="pt_br">
  <meta http-equiv="Description" content="Escola de Engenharia de São Carlos da Universidade de São Paulo - USP">
  <meta http-equiv="Keywords" content="Escola de Engenharia de São Carlos, EESC, Universidade de São Paulo, USP, São Carlos, Engenharia, Mecânica, Elétrica, Hidráulica, Civil, Produção, Tecnologia, Pesquisa, Inovação, Graduação, Pós-Graduação, Cultura, Alunos, Universidade, Pós-Graduação, Biblioteca">
  <link rel="stylesheet" type="text/css" href="./includes/styles.css">

 </head>

 <body>
  <div id="general">
   <div id="bar"></div>
   <div id="header"></div>
   <div id="menu">
    <a href="./index.php">Home</a>&nbsp;|&nbsp;
    <a href="./programacao.php">Programação</a>&nbsp;|&nbsp;
<!--    <a href="./inscricoes.php">Inscrições</a>&nbsp;|&nbsp; -->
    <a href="./infos_gerais.php">Informações Gerais</a>&nbsp;|&nbsp;
    <a href="./downloads.php">Downloads</a>&nbsp;&nbsp;
   </div>
   <div id="content">
