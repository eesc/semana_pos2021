<?php
//----------------------------------------------
//
//  Esta função possui a finalidade de realizar um 
//  tratamento em aspas simples e duplas para que
//  estas sejam tratadas como caracteres simples e
//  não como caracteres especiais.
//
function p_aspas($string)
{
   $string = stripslashes($string);
   $string = htmlspecialchars($string, ENT_QUOTES);
   return $string;	
}

//--------------------------------------
//
//  Esta função tem a finalidade de transformar quebras de
//  linhas e espaços em branco no inicio das frases em
//  tags HTML imprimiveis.
//
//Obs: Para que as quebras de linha funcionem corretamente, 
//     é necessário que se insira wrap="PHYSICAL" no textarea
//     para digitação dos dados.
//
function p_espacos($string)
{
     $palavra = split("\n",$string);
   
	  //zera string
	 $string=''; 
	 
	 foreach ($palavra as $d)
	 {
	     
          $i=0;
          while(($i<strlen($d))&&($d[$i]==" "))
	      {
	           $string .= '&nbsp;';
		       $i++; 		
		  }
          while ($i<strlen($d))
          {
	         $string.=$d[$i];
		     $i++;
		  }	     
		  $string .='<br>';
	 }
   $string = ereg_replace("<br>$","",$string);
   
   return $string;
}
//-------------------------------------------

function p_especiais($string)
{
   return p_espacos(p_aspas($string));
}

//-------------------------------------------

function p_valor($valor,$pSifrao=true)
{
   $pos = strpos(ereg_replace (",", ".",$valor),".");
   if (!is_integer($pos))
      $pos = strlen($valor);
   $inteiro = substr($valor,0,$pos);
   if (strlen($inteiro)==0)
      $inteiro = '0';
   $fracao = substr($valor,$pos+1,strlen($valor));
   if (strlen($fracao)==0)
      $fracao .= '00';
   if (strlen($fracao)==1)
      $fracao .= '0';
	
   if ($pSifrao)
      return "R\$ $inteiro,$fracao";
   else
      return "$inteiro,$fracao";
}

//-------------------------------------------

function p_valor_sql($valor)
{
   return ereg_replace (",", ".",$valor);
}

?>