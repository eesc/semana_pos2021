<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

 <head>
  <title>XVII Semana da Pós-Graduação na EESC e II Semana da Pós-Graduação do ICMC</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta http-equiv="Content-Language" content="pt_br">
  <meta http-equiv="Description" content="Escola de Engenharia de São Carlos da Universidade de São Paulo - USP">
  <meta http-equiv="Keywords" content="Escola de Engenharia de São Carlos, EESC, Universidade de São Paulo, USP, São Carlos, Engenharia, Mecânica, Elétrica, Hidráulica, Civil, Produção, Tecnologia, Pesquisa, Inovação, Graduação, Pós-Graduação, Cultura, Alunos, Universidade, Pós-Graduação, Biblioteca">
  <style type="text/css">
  body
  {
        background: #87ceff;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
  }

  .titulo
  {
	font-family:Arial, Helvetica, sans-serif;
 	font-size: 14px;
  }
  </style>
 </head>
 <body>
 <p align="center" class="titulo"><b>{TITULO}</b></p>
 <p>Palestrante: <b>{PALESTRANTE}</b><br>
 Dia: <b>{DATA}</b><br>
 Horário: <b>{HORAINICIO} - {HORAFIM}</b><br>
 Local: <b>{LOCAL}</b></p>
 <p><b>{RESUMO}</b></p>
 <p align="right"><input type="button" value="Fechar" onclick="self.close()"></p>
 </body>
</html>
