<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

 <head>
  <title>XVII Semana da Pós-Graduação na EESC e II Semana da Pós-Graduação do ICMC</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta http-equiv="Content-Language" content="pt_br">
  <meta http-equiv="Description" content="Escola de Engenharia de São Carlos da Universidade de São Paulo - USP">
  <meta http-equiv="Keywords" content="Escola de Engenharia de São Carlos, EESC, Universidade de São Paulo, USP, São Carlos, Engenharia, Mecânica, Elétrica, Hidráulica, Civil, Produção, Tecnologia, Pesquisa, Inovação, Graduação, Pós-Graduação, Cultura, Alunos, Universidade, Pós-Graduação, Biblioteca">
  <link rel="stylesheet" type="text/css" href="includes/styles.css">
  <link rel="stylesheet" type="text/css" href="chrometheme/chromestyle.css" />
  <script type="text/javascript" src="chromejs/chrome.js"></script>
 </head>

 <body>
  <div id="general">
   <div id="bar"></div>
   <div id="header"></div>
   <div class="chromestyle" id="chromemenu">
    <ul>
     <li><a href="./index.php">Home</a></li>
     <li><a href="./programacao.php">Programa&ccedil;&atilde;o</a></li>
     <li><a href="#" rel="dropmenu1">Inscri&ccedil;&atilde;o</a></li>
     <li><a href="./infos_gerais.php">Informa&ccedil;&otilde;es Gerais</a></li>
     <li><a href="./downloads.php">Downloads</a></li>
     <li><a href="./links_gerais.php">Links</a></li>
     <li><a href="#" rel="dropmenu2">Edi&ccedil;&otilde;es Anteriores</a></li>
     <li><a href="./contato.php">Contato</a></li>
    </ul>
   </div>                                                  
   <div id="dropmenu1" class="dropmenudiv">
    <a href="./inscricoes.php">Presencial</a>
    <a href="./inscricoesvc.php">Videoconfer&ecirc;ncia</a>
   </div>                                                  
   <div id="dropmenu2" class="dropmenudiv" style="width: 70px;">
    <a href="http://sistemas.eesc.usp.br/semana_pos2017" target="_blank">2017</a>
    <a href="http://sistemas.eesc.usp.br/semana_pos2018" target="_blank">2018</a>
    <a href="http://sistemas.eesc.usp.br/semana_pos2019" target="_blank">2019</a>
    <a href="http://sistemas.eesc.usp.br/semana_pos2020" target="_blank">2020</a>
   </div>
   <script type="text/javascript">
    cssdropdown.startchrome("chromemenu")
   </script>
   <div id="content">
   <p align="center"><b>Inscrições - Sala de Videoconferência  </b></p>
   {PAGINA}
   <!-- BEGIN block_INSCRICOESENCERRADAS -->
   <!-- <center><font size="3" color="red"><b>Inscrições encerradas.</b></font></center> -->
   <!-- END block_INSCRICOESENCERRADAS -->
   <!-- BEGIN block_FORMULARIO -->
   <form name="form1"  method="post">

         <table align="center">
          <tr>
           <td width="30%">&nbsp;</td>
           <td><b>Eventos</b></td>
          </tr>
          <!-- BEGIN block_PROGS -->
          <tr>
           <td align="right">{CHECKBOX}</td>
           <td><a href="#" OnClick="window.open('./sinopse.php?id_prog={ID_PROG}','wndLogin','width=580,height=280,scrollbars=yes'); return false;">{TITULO}</a></td>
          </tr>
          <!-- END block_PROGS -->
          <tr>
           <td colspan="2">&nbsp;</td>
          </tr>
          <!-- BEGIN block_ERROS -->
          <tr>
           <td align="right"><img src="http://www.eesc.usp.br/eesc/administracao/sti/images/ico_error.png" border="0"></td>
           <td><font color="red">{TEXTO}</font></td>
          </tr>  
          <!-- END block_ERROS -->
	  <tr>
           <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
           <td>&nbsp;</td>
	   <td><b>Dados de Inscrição</b></td>
          </tr>
          <tr>
           <td align="right">Nome: </td>
           <td><input name="nome"  size="40" value="{NOME}"></td>
          </tr>          
          <tr>
           <td align="right">E-mail: </td>
           <td><input name="email"  size="30" value="{EMAIL}"></td>
          </tr>          
          <tr>
           <td align="right">Confirmação de E-mail: </td>

           <td><input name="conf_email"  size="30" value="{CONF_EMAIL}"></td>
          </tr>          
          <tr>
           <td align="right">CPF: </td>
           <td><input name="cpf"  size="15" maxlength="12" value="{CPF}"></td>
          </tr>          
	  <tr>
	   <td align="right">Telefone: </td>
           <td><input name="telefone"  size="10" maxlength="10" value="{TELEFONE}"></td>
	  </tr>
	  <tr>
	   <td align="right">Cidade: </td>
           <td><input name="cidade"  size="25"  value="{CIDADE}"></td>
	  </tr>
	  <tr>
	   <td align="right">Profissão: </td>
           <td><input name="profissao"  size="25"  value="{PROFISSAO}"></td>
	  </tr>          
          <tr><td colspan="2">&nbsp;</td></tr>            
          <tr>
           <td align="right" valign="top">Categoria: </td>

           <td><input type="radio" name="categoria" value="Aluno de Pós-Graduação em Ciências da Engenharia Ambiental"> Aluno de Pós-Graduação em Ciências da Engenharia Ambiental<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Engenharia Civil (Estruturas)"> Aluno de Pós-Graduação em Engenharia Civil (Estruturas)<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Engenharia de Produção"> Aluno de Pós-Graduação em Engenharia de Produção<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Engenharia de Transportes"> Aluno de Pós-Graduação em Engenharia de Transportes<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Engenharia Elétrica"> Aluno de Pós-Graduação em Engenharia Elétrica<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Engenharia Hidráulica e Saneamento"> Aluno de Pós-Graduação em Engenharia Hidráulica e Saneamento<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Engenharia Mecânica"> Aluno de Pós-Graduação em Engenharia Mecânica<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Geotecnia"> Aluno de Pós-Graduação em Geotecnia<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação Interunidades em Bioengenharia"> Aluno de Pós-Graduação Interunidades em Bioengenharia<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Ciência e Engenharia de Materiais"> Aluno de Pós-Graduação em Ciência e Engenharia de Materiais<br>
               <input type="radio" name="categoria" value="Aluno de Pós-Graduação em Mestrado Profissional em Rede Nacional para Ensino das Ciências Ambientais"> Aluno de Pós-Graduação em Mestrado Profissional em Rede Nacional para &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ensino das Ciências Ambientais<br>
               <input type="radio" name="categoria" value="Aluno de Graduação"> Aluno de Graduação<br>
               <input type="radio" name="categoria" value="Docente"> Docente<br>
               <input type="radio" name="categoria" value="Funcionário"> Funcionário<br>
               <input type="radio" name="categoria" value="Outro"> Outro: <input type="text" name="nome_categoria" size="60"></td>

          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>            
          <tr>

           <td align="right" valign="top">Procedência: </td>
           <td><input type="radio" name="procedencia" value="USP"> USP<br>
                    <input type="radio" name="procedencia" value="Outro"> Outra: <input type="text" name="nome_procedencia" size="13"></td>
          </tr>           
         </table>
         <p align="center">
          <input type="submit" name="Enviar" value="Enviar dados">

         </p>
        </form>
	<!-- END block_FORMULARIO -->
	<!-- BEGIN block_ENVIADO -->
	<p style="margin-left: 20px;">Sua inscrição foi realizada com sucesso.<br>
	Dentro de instantes, você receberá um e-mail confirmando os dados de sua inscrição.<br>
	Obrigado.</p>
	<!-- END block_ENVIADO -->
   </div>
   <div id="realizacao">
    <b>Realização:</b><br><br>
    <a href="http://www.eesc.usp.br/" target="_blank"><img src="./images/logo_eesc_vertical1050.jpg" border="0"></a>&nbsp;|&nbsp;
    <a href="http://www.eesc.usp.br/biblioteca" target="_blank"><img src="./images/SBINovo_d.png" border="0"></a>&nbsp;|&nbsp;
    <a href="http://www.eesc.usp.br/institucional/colegiados.php" target="_blank"><img src="./images/CPG3.png" border="0"></a>&nbsp;|&nbsp;
    <a href="http://www.icmc.usp.br/pos-graduacao" target="_blank"><img src="./images/logo50_icmc_50anos.jpg" border="0"></a>&nbsp;&nbsp;
   </div>
   <div id="apoio">
    <b>Patrocínio:</b><br><br>
    <p align="center"><img src="images/incentivo/pat1.gif" width="350px"></p>
   </div>
   <div id="apoio">
    <b>Apoio:</b><br><br>
    <p align="center">{A1}</p>
   </div>
   <div id="footer">
    XVII Semana da Pós-Graduação na EESC<br> e II Semana da Pós-Graduação do ICMC<br>Escola de Engenharia de São Carlos - USP<br>Seção Técnica de Informática
   </div>
  </div>
 </body>

</html>
