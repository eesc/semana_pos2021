<?php

//incluindo o arquivo do fpdf
require_once("fpdf.php");
define('FPDF_FONTPATH','/fpdf/font/');
class MeuFPDF extends FPDF
{
     function Header()
     {
            $this->SetFont('Arial','B', 12);
            $this->Cell(0, 10, 'UNIVERSIDADE DE SÃO PAULO',0,1,'C');
            $this->Cell(0, 10, 'ESCOLA DE ENGENHARIA DE SÃO CARLOS - ScEstag',0,1,'C');
            $this->Cell(0, 10, 'ÍNDICE DE EMPRESAS - PERÍODO DO CONVÊNIO',0,1,'C');
            $this->Ln(08);

            $this->SetFont('Arial','B',10);
            $this->SetX(20);
            $this->Cell(5,5,'NOME');
            $this->SetX(158);
            $this->Cell(60,5,'PERÍODO CONVÊNIO');
            $this->Ln(02);

     }
     function Footer()
     {
            $this->SetY(-15);
            $this->SetFont('Arial','I',10);
            $this->Cell(0,10,'Página '.$this->Pageno().'/{nb}',0,0,'C');
     }
}


include_once("../includes/valida_cookies.inc");
include_once("../includes/header_footer.inc");


if ($opc_ordem == 'nome')
   $query = "SELECT * FROM empresas order by nome";
elseif ($opc_ordem == 'inicio')
   $query = "SELECT * FROM empresas order by inicioconvenio,nome";
elseif ($opc_ordem == 'termino')
   $query = "SELECT * FROM empresas order by terminoconvenio,nome";
else
   $query = "SELECT * FROM empresas order by nome";

$result = mysql_query($query);

// Variaveis de Tamanho

$mesq = "5"; // Margem Esquerda (mm)
$mdir = "5"; // Margem Direita (mm)
$msup = "22"; // Margem Superior (mm)
$leti = "279,4"; // Largura da Etiqueta (mm)
$aeti = "215,9"; // Altura da Etiqueta (mm)
$ehet = "6,2"; // Espaço horizontal entre as Etiquetas (mm)
$pdf=new MeuFPDF('P','mm','Letter'); // Cria um arquivo novo tipo carta, na vertical.
$pdf->Open(); // inicia documento
$pdf->AliasNbPages(); // 
$pdf->AddPage(); // adiciona a primeira pagina
$pdf->SetMargins('5','12,7'); // Define as margens do documento
$pdf->SetAuthor("Jonas Ferreira"); // Define o autor
//$pdf->SetFont('helvetica','',7); // Define a fonte
$pdf->SetFont('helvetica','',10); // Define a fonte
$pdf->SetDisplayMode('fullwidth');

$linha = 0;

//  $pdf->SetFont('Arial','B',10);
//  $pdf->Cell(5,5,'NOME');
//  $pdf->SetX(150);
//  $pdf->Cell(60,5,'PERÍODO CONVÊNIO');

//MONTA A ARRAY
while($dados = mysql_fetch_array($result)) {
$nome = $dados["nome"];
$inicio_convenio = $dados["inicioconvenio"];
$fim_convenio = $dados["terminoconvenio"];

$inicio_convenio = $dados["inicioconvenio"];
 $k_data = explode("-", $inicio_convenio, 3);
 $k_ano = $k_data[0];
 $k_mes = $k_data[1];
 $k_dia = $k_data[2];
$inicio_convenio = $k_dia."/".$k_mes."/".$k_ano;   

$fim_convenio = $dados["terminoconvenio"];
 $k_data = explode("-", $fim_convenio, 3);
 $k_ano = $k_data[0];
 $k_mes = $k_data[1];
 $k_dia = $k_data[2];
$fim_convenio = $k_dia."/".$k_mes."/".$k_ano;   


if($linha == "42") {
  $pdf->AddPage();
  $linha = 0;
}

$pdf->ln();
$pdf->SetX(20);
$pdf->Cell(35,5,$nome);
//if($inicio_convenio <> 0)
if($dados["inicioconvenio"] <> 0)
  {
   $pdf->SetX(158);
   $pdf->Cell(80,5,$inicio_convenio);
  }
//if($fim_convenio <> 0)  - - dia 00 aparecia em branco no relatorio
if($dados["terminoconvenio"] <> 0)
  {
   $pdf->SetX(178);
   $pdf->Cell(80,5,$fim_convenio);
  }
$linha = $linha +1; // $linha é igual ela mesma +1


$coluna = $coluna+1;
}

if($opc_saida == 'arquivo')
  $pdf->Output("rel6empresa.pdf",true);

$pdf->Output();

?>
