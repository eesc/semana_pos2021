<?php

include_once("../includes/valida_cookies.inc");
include_once("../includes/header_footer.inc");
//include_once("../includes/db_vars.inc");

echo $header;


if ($_POST['submit'] == 'Gravar') {
  if(($_POST['nome'] <> '')) {

   $nome = $_POST['nome'];
   $depto = $_POST['depto'];
   $empresa_estagio = $_POST['empresa_estagio'];

//  CONSISTENCIA DATA INICIO ESTAGIO
   $inicio_estagio = $_POST['inicio_estagio'];
   $k_data = explode("/", $inicio_estagio, 3);
   $k_dia = $k_data[0];
   $k_mes = $k_data[1];
   $k_ano = $k_data[2];

//   if ($inicio_estagio != " " && $inicio_estagio != NULL){ 
//     $var_data = checkdate((int)$k_mes, (int)$k_dia, (int)$k_ano);
//      if(!$var_data) {
//         echo "DATA INICIO ESTAGIO INVALIDA";  
//
//         $php_self = $_SERVER['PHP_SELF'];}}

   $inicio_estagio = $k_ano."-".$k_mes."-".$k_dia;   

//  CONSISTENCIA DATA FIM ESTAGIO
   $fim_estagio = $_POST['fim_estagio'];
   $k_data = explode("/", $fim_estagio, 3);
   $k_dia = $k_data[0];
   $k_mes = $k_data[1];
   $k_ano = $k_data[2];
   $fim_estagio = $k_ano."-".$k_mes."-".$k_dia;

   $escola_codigo = $_POST['escola_codigo'];
   $codigo_docente = $_POST['codigo_docente'];
   $tipo_estagio = $_POST['tipo_estagio'];
   $observacoes = $_POST['observacoes'];

    $query = "INSERT INTO estagiarios 
                     (codigo,
			  nome,
			  depto,
			  empresa_estagio,
			  inicio_estagio,
			  fim_estagio,
			  escola_codigo,
			  codigo_docente,
			  tipo_estagio,
                       observacoes) 
                    VALUES
		        (NULL,
			  '$nome',
			  '$depto',
			  '$empresa_estagio',
			  '$inicio_estagio', 
			  '$fim_estagio',
			  '$escola_codigo',
			  '$codigo_docente',
			  '$tipo_estagio',
			  '$observacoes')";
    $result = mysql_query($query);
 
    if (!$result) {
      $feedback = 'ERRO NO BANCO DE DADOS -  ESTAGIARIOS!';
    }
  } else {
      echo "Campo obrigatorio nao preenchido!";
  } 
   $nome = "";
   $depto = "";
   $empresa_estagio = "";
   $inicio_estagio = "";
   $fim_estagio = "";
   $escola_codigo = "";
   $codigo_docente = "";
   $tipo_estagio = "";
   $observacoes = "";
}

// Gerar array de docentes

$ssql = "select codigo, nome from docentes order by nome";
$result_docentes=mysql_query($ssql); 
    if (!$result_docentes) {
      $feedback = 'ERRO SELEÇÃO NO CADASTRO DE DOCENTES!';
    }

$bg = 1;

while ($row_array = mysql_fetch_array($result_docentes)) {
  if ($bg == 1)
    $color = '#f1eeca';
  else
    $color = '#dcdde5';

    $bg = 1 - $bg;

  $vcodigo = $row_array['codigo'];
  $vnome = $row_array['nome'];

  $return_string_docentes .= "<option value=\"$vcodigo\">$vnome</option>;";
}


// Gerar array de empresas

$ssql = "select codigo, nome from empresas order by nome";
$result_empresa=mysql_query($ssql); 
    if (!$result_empresa) {
      $feedback = 'ERRO SELEÇÃO NO CADASTRO DE EMPRESAS!';
    }

$bg = 1;
$return_string .= "<option value=\"\">'Selecione uma Empresa'</option>;";

while ($row_array = mysql_fetch_array($result_empresa)) {
  if ($bg == 1)
    $color = '#f1eeca';
  else
    $color = '#dcdde5';

    $bg = 1 - $bg;

  $vcodigo = $row_array['codigo'];
  $vnome = $row_array['nome'];

  $return_string .= "<option value=\"$vcodigo\">$vnome</option>;";
}


// Gerar array de instituicoes de ensino

$ssql = "select codigo, nome from entidades_ensino order by nome";
$result_iensino=mysql_query($ssql); 
    if (!$result_iensino) {
      $feedback = 'ERRO SELEÇÃO NO CADASTRO DE ENTIDADES DE ENSINO!';
    }

$bg = 1;

while ($row_array = mysql_fetch_array($result_iensino)) {
  if ($bg == 1)
    $color = '#f1eeca';
  else
    $color = '#dcdde5';

    $bg = 1 - $bg;

  $vcodigoie = $row_array['codigo'];
  $vnomeie = $row_array['nome'];

  $return_string_ie .= "<option value=\"$vcodigoie\">$vnomeie</option>;";
}

$php_self = $_SERVER['PHP_SELF'];
echo "
      <form name=\"form1\" method=\"POST\" action=\"$php_self\" enctype=\"multipart/form-data\">
  <table width=\"80%\" align=\"center\" border=1>
    <tr bgcolor=\"dcdde5\">
     <td align=\"center\" colspan=\"4\" >CADASTRO DE ESTAGIÁRIOS</td>
    </tr>

    <tr bgcolor=\"dcdde5\">
     <td align=\"right\">Nome</td>
     <td><input type=\"text\" name=\"nome\" size=\"50\" value=\"$nome\" maxlength=\"50\"></td>
    </tr>

    <tr bgcolor=\"dcdde5\">
      <td align=\"right\">Departamento</td>
      <td><input type=\"text\" name=\"depto\" size=\"70\" value=\"$depto\" maxlength=\"25\"></td>
    </tr>

    <tr bgcolor=\"f1eeca\">
        <td align=\"right\">Empresa estágio</td>

        <td>
           <select name=\"empresa_estagio\" size=\"1\">
            $return_string
           </select>
      </td>
    </tr>
    <tr bgcolor=\"dcdde5\">
      <td align=\"right\">Inicio estágio</td>
      <td><input type=\"text\" name=\"inicio_estagio\" size=\"10\" value=\"$inicio_estagio\"></td>
    </tr>
    <tr bgcolor=\"f1eeca\">
      <td align=\"right\">Fim estágio</td>
      <td><input type=\"text\" name=\"fim_estagio\" size=\"10\" value=\"$fim_estagio\"></td>
    </tr>

    <tr bgcolor=\"dcdde5\">
      <td align=\"right\">Instituição de ensino</td>
        <td>
           <select name=\"escola_codigo\" size=\"1\">
            $return_string_ie
           </select>
      </td>
    </tr>


    <tr bgcolor=\"dcdde5\">
      <td align=\"right\">Orientador</td>
        <td>
           <select name=\"codigo_docente\" size=\"1\">
            $return_string_docentes
           </select>
      </td>
    </tr>

    <tr bgcolor=\"dcdde5\">
      <td align=\"right\">Tipo estágio</td>
      <td><input type=\"text\" name=\"tipo_estagio\" size=\"12\" value=\"$tipo_estagio\"></td>
    </tr>


    <tr bgcolor=\"dcdde5\">
      <td align=\"right\">Observações </td>
      <td><textarea rows=\"8\" name=\"observacoes\" cols=\"120\"></textarea></td>
    </tr>


  </tbody>
</table>
   <p align=\"center\"><INPUT type=\"submit\" name=\"submit\" value=\"Gravar\"></p>
     </form>
";

echo $footer;

?>

