<?php

// incluindo o arquivo do fpdf
require_once("fpdf.php");
define('FPDF_FONTPATH','/fpdf/font/');

class MeuFPDF extends FPDF

{
     function Header()
     {
            $this->SetFont('Arial','B', 12);
            $this->Cell(0, 10, 'UNIVERSIDADE DE SÃO PAULO',0,1,'C');
            $this->Cell(0, 10, 'ESCOLA DE ENGENHARIA DE SÃO CARLOS - ScEstag',0,2,'C');
            $this->Cell(0, 10, 'RELATÓRIO DE ESTAGIÁRIOS',0,2,'C');
            $this->Cell(0, 10, 'ORIENTADOR: $nome_orientador',0,2,'C');
            $this->Cell(0, 10, 'ANO: $t_ano',0,2,'C');
            $this->Ln(01);
            $this->SetFont('Arial','B',10);
            $this->Cell(5,5,'CÓDIGO');
            $this->SetX(30);
            $this->Cell(60,5,'NOME');
            $this->SetX(124);
            $this->Cell(40,5,'UNIDADE');
            $this->SetX(145);
            $this->Cell(40,5,'DEPTO');
            $this->SetX(164);
            $this->Cell(40,5,'PERÍODO');


            $this->Ln(08);
     }
     function Footer()
     {
            $this->SetY(-15);
            $this->SetFont('Arial','I',10);
            $this->Cell(0,10,'Página '.$this->Pageno().'/{nb}'.' - '.date('d/m/Y').' - '.date('H:i'),0,0,'C');
     }
}


include_once("../includes/valida_cookies.inc");
include_once("../includes/header_footer.inc");

if (empty($codigo_depto) and empty($condtipo) and empty($regime) and !empty($condativo))
//   {
//   echo "1 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE entidade = \"$codigo_entidade\" and
                                                    condativo = \"$condativo\" and
                                                    unidade = \"$codigo_unidade\" order by entidade, unidade, depto, nome
                      ");
//   }
elseif (empty($codigo_depto) and !empty($condtipo) and empty($regime) and !empty($condativo))
//   {
//   echo "2 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE condativo = \"$condativo\" and
                                                    entidade = \"$codigo_entidade\" and
                                                    condtipo = \"$condtipo\" and
                                                    unidade = \"$codigo_unidade\" order by entidade, unidade, depto, nome
                      ");
//   }
elseif (empty($codigo_depto) and !empty($condtipo) and empty($regime) and empty($condativo))
//   {
//   echo "3 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE entidade = \"$codigo_entidade\" and
                                                    condtipo = \"$condtipo\" and
                                                    unidade = \"$codigo_unidade\" order by entidade, unidade, depto, nome
                      ");
//   }
elseif (empty($codigo_depto) and empty($condtipo) and empty($regime) and empty($condativo))
//   {
//   echo "4 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE entidade = \"$codigo_entidade\" and
                                                    unidade = \"$codigo_unidade\" order by entidade, unidade, depto, nome
                      ");
//   }
elseif (!empty($codigo_depto) and empty($condtipo) and empty($regime) and !empty($condativo))
//   {
//   echo "5 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE entidade = \"$codigo_entidade\" and
                                                    unidade = \"$codigo_unidade\" and
                                                    depto = \"$codigo_depto\" and
                                                    condativo = \"$condativo\"  order by entidade, unidade, depto, nome
                      ");
//   }
elseif (empty($codigo_depto) and empty($condtipo) and !empty($regime) and !empty($condativo))
//   {
//   echo "6 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE entidade = \"$codigo_entidade\" and
                                                    unidade = \"$codigo_unidade\" and
                                                    regime = \"$regime\" and
                                                    condativo = \"$condativo\"  order by entidade, unidade, depto, nome
                      ");
//   }
elseif (empty($codigo_depto))
//   {
//   echo "7 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE condativo = \"$condativo\" and
                                                    entidade = \"$codigo_entidade\" and
                                                    unidade = \"$codigo_unidade\" and
                                                    condtipo = \"$condtipo\" and
                                                    regime = \"$regime\" order by entidade, unidade, depto, nome
                      ");
//   }
else
//   {
//   echo "8 Seleciona ativo inativo de uma entidade e de uma unidade";
   $busca = mysql_query( "SELECT * FROM clientes WHERE condativo = \"$condativo\" and
                                                    entidade = \"$codigo_entidade\" and
                                                    unidade = \"$codigo_unidade\" and
                                                    depto = \"$codigo_depto\" and
                                                    condtipo = \"$condtipo\" and
                                                    regime = \"$regime\" order by entidade, unidade, depto, nome
                      ");
//   }

// Variaveis de Tamanho

$mesq = "5"; // Margem Esquerda (mm)
$mdir = "5"; // Margem Direita (mm)
$msup = "22"; // Margem Superior (mm)
$leti = "279,4"; // Largura da Etiqueta (mm)
$aeti = "215,9"; // Altura da Etiqueta (mm)
$ehet = "6,2"; // Espaço horizontal entre as Etiquetas (mm)
$pdf=new MeuFPDF('P','mm','Letter'); // Cria um arquivo novo tipo carta, na vertical.
$pdf->Open(); // inicia documento
$pdf->AliasNbPages(); // 
$pdf->AddPage(); // adiciona a primeira pagina
$pdf->SetMargins('5','12,7'); // Define as margens do documento
$pdf->SetAuthor("Jonas Ferreira"); // Define o autor
$pdf->SetFont('helvetica','',7); // Define a fonte
$pdf->SetDisplayMode('fullwidth');

$linha = 0;

//MONTA A ARRAY
while($dados = mysql_fetch_array($busca)) {
$codigo = $dados["codigo"];
$nome = $dados["nome"];
$entidade = $dados["entidade"];
$unidade = $dados["unidade"];
$depto = $dados["depto"];
$ende = $dados["endereco"];
$complemento = $dados["complemento"];
if (!empty($dados["fone"]))
   $fone =  "Telefone: ". $dados["fone"];
else
   $fone = "";

if (!empty($dados["fax"]))
   $fax =  "FAX: ". $dados["fax"];
else
   $fax = "";

if (!empty($dados["email"]))
   $email =  "E-mail: ". $dados["email"];
else
   $email = "";
$regime = $dados["regime"];
// cliente tipo - Docente/Funcionario
   $condtipo = $dados['condtipo'];
   if ($condtipo == 'D')
      $nometipo = "DOCENTE";
   if ($condtipo == 'F')
      $nometipo = "FUNCIONÁRIO";

// Cliente - Ativo/Inativo
   $condativo = $dados['condativo'];
   if ($condativo == 'A')
      $nomecond = "ATIVO";
   if ($condativo == 'I')
      $nomecond = "INATIVO";

   $funcao = $dados['funcao'];

$fonefaxemail = $fone . " " . $fax . " " . $email;
$cida = $dados["cidade"];
$estado = $dados["estado"];
$local = $bairro . " - " . $complemento;
$cep = "CEP: " . substr($dados["cep"],0,5)."-".substr($dados["cep"],5,3) ."  ". $cida . " - " . $estado ;

// --------------------------------------------------------------------------------------------------------------------------------------
//  buscar nome entidade
if ($entidade <> 0)
{
      $query_entidade = "SELECT * FROM entidades WHERE codigo = $entidade";
      $result_entidade = mysql_query($query_entidade);
      $erro_entidade = mysql_errno();
      $msg_erro = mysql_error();
      if (!$result_entidade)
        {
         $nome_entidade = "* * *"; 
         $sigla_entidade = " "; 
         $feedback = 'ERRO NO BANCO DE DADOS - entidades!';
         if ($erro_entidade <> 0)
            echo "ERRO $erro_entidade:  $msg_erro (Entidade não selecionada)";
         else  
            echo "ERRO $erro_entidade: msg erro 0";
      }
      else
      {
       $dados_entidade = mysql_fetch_array($result_entidade);
       $nome_entidade = $dados_entidade['nome']; 
       $sigla_entidade = $dados_entidade['sigla']; 
       }
}
else
{
   $nome_entidade = "* * *"; 
   $sigla_entidade = "* * *"; 
}


// --------------------------------------------------------------------------------------------------------------------------------------
//  buscar nome unidade
if ($unidade <> 0)
{
      $query_unidade = "SELECT * FROM unidades WHERE codigo = $unidade";
      $result_unidade = mysql_query($query_unidade);
      $erro_unidade = mysql_errno();
      $msg_erro = mysql_error();
      if (!$result_unidade)
        {
         $nome_unidade = "* * *"; 
         $sigla_unidade = " "; 
         $feedback = 'ERRO NO BANCO DE DADOS - unidades!';
         if ($erro_unidade <> 0)
            echo "ERRO $erro_unidade:  $msg_erro (unidade não selecionada)";
         else  
            echo "ERRO $erro_unidade: msg erro 0";
      }
      else
      {
       $dados_unidade = mysql_fetch_array($result_unidade);
       $nome_unidade = $dados_unidade['nome']; 
       $sigla_unidade = $dados_unidade['sigla']; 
       }
}
else
{
   $nome_unidade = "* * *"; 
   $sigla_unidade = "* * *"; 
}


// --------------------------------------------------------------------------------------------------------------------------------------
//  buscar nome depto
if ($depto <> 0)
{
      $query_depto = "SELECT * FROM deptos WHERE codigo = $depto";
      $result_depto = mysql_query($query_depto);
      $erro_depto = mysql_errno();
      $msg_erro = mysql_error();
      if (!$result_depto)
        {
         $nome_depto = "* * *"; 
         $sigla_depto = " "; 
         $feedback = 'ERRO NO BANCO DE DADOS - deptos!';
         if ($erro_depto <> 0)
            echo "ERRO $erro_depto:  $msg_erro (depto não selecionado)";
         else  
            echo "ERRO $erro_depto: msg erro 0";
      }
      else
      {
       $dados_depto = mysql_fetch_array($result_depto);
       $nome_depto = $dados_depto['nome']; 
       $sigla_depto = $dados_depto['sigla']; 
       }
}
else
{
   $nome_depto = "* * *"; 
   $sigla_depto = "* * *"; 
}


// --------------------------------------------------------------------------------------------------------------------------------------
//  buscar nome funcao
if ($funcao <> 0)
{
      $query_funcao = "SELECT * FROM funcao WHERE codigo = $funcao";
      $result_funcao = mysql_query($query_funcao);
      $erro_funcao = mysql_errno();
      $msg_erro = mysql_error();
      if (!$result_funcao)
        {
         $nome_funcao = "* * *"; 
         $familia = " "; 
         $feedback = 'ERRO NO BANCO DE DADOS - função!';
         if ($erro_funcao <> 0)
            echo "ERRO $erro_funcao:  $msg_erro (função não selecionado)";
         else  
            echo "ERRO $erro_funcao: msg erro 0";
      }
      else
      {
       $dados_funcao = mysql_fetch_array($result_funcao);
       $nome_funcao = $dados_funcao['nomefuncao']; 
       $familia = $dados_funcao['familia']; 
       }
}
else
{
   $nome_funcao = "* * *"; 
   $familia = " "; 
}

$origem = $sigla_entidade."/".$sigla_unidade."/".$sigla_depto;



if($linha == "38") {
  $pdf->AddPage();
  $linha = 0;
}

$pdf->ln();
$pdf->Cell(35,5,$codigo);
$pdf->SetX(30);
$pdf->Cell(80,5,$nome);
//$pdf->SetX(95);
//$pdf->Cell(40,5,$sigla_entidade);
$pdf->SetX(124);
$pdf->Cell(40,5,$sigla_unidade);
$pdf->SetX(140);
$pdf->Cell(40,5,$sigla_depto);
$pdf->SetX(164);
$pdf->Cell(40,5,$nometipo);
$pdf->SetX(190);
$pdf->Cell(40,5,$nomecond);

$pdf->ln();
$pdf->SetX(30);
$pdf->Cell(80,5,$ende);
$pdf->SetX(95);
$pdf->Cell(80,5,$local);
$pdf->SetX(124);
$pdf->Cell(40,5,$cep);

$pdf->ln();
$pdf->SetX(30);
$pdf->Cell(80,5,$familia.' - '.$nome_funcao.' / '.$regime);
//$pdf->SetX(124);
//$pdf->Cell(40,5,$nome_funcao);

$pdf->ln();
$pdf->SetX(30);
$pdf->Cell(80,5,$fonefaxemail);

$linha = $linha +4; // $linha é igual ela mesma +4


$coluna = $coluna+1;
}

//$pdf->Output("svpesrel.pdf",true);
$pdf->Output();


?>
