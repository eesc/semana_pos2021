<?php

//include("../fpdf.php"); //Include mPDF Class 
//include("../mpdf.php"); //Include mPDF Class 

require("fpdf.php");

$pdf=new FPDF(); // Create new mPDF Document

//Beginning Buffer to save PHP variables and HTML tags
ob_start(); 

// Function Date
$day = date('d');
$month = date('m');
$year = date('Y');

switch ($month)
{ 
case 1: $month = "January"; break;
case 2: $month = "February"; break;
case 3: $month = "March"; break;
case 4: $month = "April"; break;
case 5: $month = "May"; break;
case 6: $month = "June"; break;
case 7: $month = "July"; break;
case 8: $month = "August"; break;
case 9: $month = "September"; break;
case 10: $month = "October"; break;
case 11: $month = "November"; break;
case 12: $month = "December"; break;
}

echo "Hello World
Today is $month $day, $year";

$html = ob_get_contents();
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$pdf->WriteHTML(utf8_encode($html)); 

$content = $pdf->Output('', 'S');

$content = chunk_split(base64_encode($content));
$mailto = 'nome@email.com'; //Mailto here
$from_name = 'nome'; //Name of sender mail
$from_mail = 'nome@email.com'; //Mailfrom here
$subject = 'teste de envio de certificados via email'; 
$message = 'Certificado enviado com sucesso';
$filename = "certificado".date("d-m-Y_H-i",time()); //Your Filename with local date and time

//Headers of PDF and e-mail
$boundary = "XYZ-" . date("dmYis") . "-ZYX"; 

$header = "--$boundary\r\n"; 
$header .= "Content-Transfer-Encoding: 8bits\r\n"; 
$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n\r\n"; // or utf-8
$header .= "$message\r\n";
$header .= "--$boundary\r\n";
$header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n";
$header .= "Content-Transfer-Encoding: base64\r\n\r\n";
$header .= "$content\r\n"; 
$header .= "--$boundary--\r\n";

$header2 = "MIME-Version: 1.0\r\n";
$header2 .= "From: ".$from_name." \r\n"; 
$header2 .= "Return-Path: $from_mail\r\n";
$header2 .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
$header2 .= "$boundary\r\n";

mail($mailto,$subject,$header,$header2, "-r".$from_mail);

$pdf->Output($filename ,'I');
exit;

?>