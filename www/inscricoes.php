<?php

require_once("application_top.php");
require("phpmailer/class.phpmailer.php");

$tpl = new Template(".");
$tpl->set_file("main","inscricoes.tpl");
$tpl->set_block("main","block_ENVIADO","bENVIADO");
$tpl->set_block("main","block_FORMULARIO","bFORMULARIO");
$tpl->set_block("block_FORMULARIO","block_PROGS","bPROGS");
$tpl->set_block("block_FORMULARIO","block_ERROS","bERROS");
$tpl->set_block("main","block_INSCRICOESENCERRADAS","bINSCRICOESENCERRADAS");
// SORTEIO DO APOIO :: ver application_top.php
//$tpl->set_var("BANNER",$virus);
//$tpl->set_var("A1","<img src=\"images/incentivo/apoio/$a1\" border=\"0\" height=\"85\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a2\" border=\"0\" height=\"85\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a3\" border=\"0\" height=\"85\" >");
//$tpl->set_var("A1","<img src=\"images/incentivo/apoio/$a1\" border=\"0\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a2\" border=\"0\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a3\" border=\"0\" >
//        &nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a4\" border=\"0\">&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a5\" border=\"0\" >");
$tpl->set_var("A1","<img src=\"images/incentivo/apoio/$a1\" border=\"0\" height=\"60\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a2\" border=\"0\" height=\"60\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a3\" border=\"0\" height=\"60\" >
        &nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a4\" border=\"0\" height=\"60\" >&nbsp;&nbsp;<img src=\"images/incentivo/apoio/$a5\" border=\"0\" height=\"60\" >
");



$query = new dbSqlConnectionBIBLIOTECA();
$query->query("SELECT pagina FROM semanapos_inscr_campanha_n WHERE ano = '2021'");
$query->next_record();
$campanha = $query->f('pagina');

$query = new dbSqlConnectionBIBLIOTECA();
$query->query("SELECT pagina FROM semanapos_inscr_conteudo_n WHERE ano = '2021'");
$query->next_record();
$tpl->set_var("PAGINA",$query->f('pagina'));



if ($Enviar)
{
   $procedencia = $procedencia != 'Outro' ? $procedencia : $nome_procedencia;
   $categoria = $categoria != 'Outro' ? $categoria : $nome_categoria;

   if (!sizeof($id_progs))
     $erros[] = "Selecione um curso para inscrição.";

   if (!Validate::string($nome, array("min_length"=> 1)))
     $erros[] = "Entre com o seu nome.";

   if (!Validate::email($email))
     $erros[] = "Entre com um e-mail válido.";
   elseif (!Validate::email($conf_email) || $email !=$conf_email)
     $erros[] = "Confirme seu e-mail.";

   if (!Validate::cpf($cpf))
     $erros[] = "Entre com um número de CPF válido.";
   else
   {
	foreach($id_progs as $id_prog)
	{
		$query = new dbSqlConnectionBIBLIOTECA();
		$query->query("SELECT p.titulo FROM semanapos_inscricoes_n i, semanapos_prog_n2021 p WHERE p.id_prog = i.id_prog AND i.id_prog = $id_prog AND (i.rg = '$cpf' OR i.email = '$email') AND i.ano = 2021");
		if ($query->next_record())
		{
			$erros[] = "Voc&ecirc; j&aacute; est&aacute; inscrito no evento <b>".$query->f('titulo')."</b>";
		}
	}
   }


//   if (!Validate::string($telefone, array("min_length" => 8)))
//     $erros[] = "Entre com seu telefone.";
//
//   if (!Validate::string($cidade, array("min_length" => 1)))
//     $erros[] = "Entre com a cidade em que mora.";
//
//   if (!Validate::string($profissao, array("min_length" => 1)))
//     $erros[] = "Entre com sua profiss&atilde;o.";

   if (!Validate::string($categoria, array("min_length"=> 1)))
     $erros[] = "Selecione uma categoria.";

   if (!Validate::string($procedencia, array("min_length"=> 1)))
     $erros[] = "Selecione uma op&ccedil;&atilde;o de proced&ecirc;ncia.";

   if ($port_necessidade_especial == 'Sim' and EMPTY($descricao_necessidade_especial))
     $erros[] = "Inconsistencia  no item de necessidade especial.";

   if ($port_necessidade_especial == 'Não' and !EMPTY($descricao_necessidade_especial))
     $erros[] = "Inconsistencia  no item de necessidade especial.";

   $email = $conf_email;

   if (!is_array($erros))
   {
	$texto = "Prezado(a) ".ucwords(strtolower($nome)).",<br>";
	foreach($id_progs as $id_prog)
	{
		$query = new dbSqlConnectionBIBLIOTECA();
		$query->query("INSERT INTO semanapos_inscricoes_n
        	        (id_prog, nome, email, rg, categoria, procedencia, port_necessidade_especial, descricao_necessidade_especial, ano, tipo, data_hora, ip, telefone, cidade, profissao) VALUES
                	($id_prog, '".ucwords(strtolower($nome))."', '".strtolower($email)."', '$cpf', '$categoria', '$procedencia', '$port_necessidade_especial', '$descricao_necessidade_especial', '2021', 'PR', '".date("Y-m-d H:i:s")."', '$REMOTE_ADDR', '$telefone', '$cidade', '$profissao')");
		$query->query("SELECT titulo FROM semanapos_prog_n2021 WHERE id_prog = $id_prog");
		$query->next_record();
		$cursos[] = $query->f('titulo');
	}
//echo "Cursos zero: $cursos[0] - ($id_progs)";
//echo "Cursos um  : $cursos[1] - ($id_progs)";
	$texto = sizeof($id_progs) > 1 ? "Confirmamos sua inscri&ccedil;&atilde;o.<br><br><br><b>".implode(", ",$cursos)."</b> a ser realizado durante a Semana da P&oacute;s na EESC.</p>".$campanha."<i>Servi&ccedil;o de Biblioteca - EESC - USP</i></p>" : "Confirmamos sua inscri&ccedil;&atilde;o:<br><br><br><b>".$cursos[0]."</b> a ser realizado durante a Semana da P&oacute;s na EESC.</p>".$campanha."<i>Servi&ccedil;o de Biblioteca - EESC - USP</i></p>";

	 $query->query("SELECT contato FROM semanapos_contato_n WHERE ano = '2021'");
	 $query->next_record();

//         $mail = new PHPMailer();
//         $mail -> SMTPDebug = 2;
//         $mail->CharSet = "utf-8";
//         $mail->From     = $query->f('contato');
//         $mail->FromName = "BIBLIOTECA - EESC/USP";
//         $mail->IsMail();
//	 $mail->IsHTML(true);
//         $mail->Subject = "Confirmação de inscrição";
//         $mail->AddAddress($email);
//         $mail->Body = emailEESC($texto);
//	 $mail->Send();
//
//
//      if(!$mail->Send()) {
//           echo "Mailer Error: " . $mail->ErrorInfo;
//        } else {
//          echo "Message sent!";
//        }

//---- inicio

$to = $email;      // aqui será o email do destinatario

$mail = new PHPMailer();

$mail->IsSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPDebug = 0;
$mail->SMTPAuth = true;
$mail->Port = '587';
$mail->SMTPSecure = 'tls';
$mail->Username = 'no-reply@eesc.usp.br';
$mail->Password = 'H3lsisb3lkd&s';
//$mail->AddReplyTo('semana@eesc.usp.br', utf8_decode('Serviço de Biblioteca EESC - Semana da Pós-Graduação'));    // aqui sera colocado os dados de quem deve receber uma resposta que o destinatario possa fazer
//$mail->AddReplyTo($email,'Serviço de Biblioteca EESC - Semana da Pós-Graduação');    // aqui sera colocado os dados de quem deve receber uma resposta que o destinatario possa fazer
//$mail->SetFrom('no-reply@eesc.usp.br','BIBLIOTECA - EESC/USP'); // Aqui é o nome do remetente do email... substitua por biblioteca ou semana da pos...
//$mail->SetFrom('semana@eesc.usp.br', utf8_decode('BIBLIOTECA - EESC/USP')); // Aqui é o nome do remetente do email... substitua por biblioteca ou semana da pos...
$mail->SetFrom('$to', utf8_decode('BIBLIOTECA - EESC/USP')); // Aqui é o nome do remetente do email... substitua por biblioteca ou semana da pos...
$mail->Subject = 'Confirmação de inscrição';    // aqui é o assunto do email. Troque pelo seu
$mail->AddAddress($to, "");
//$mail->AddAddress($to, '');
//$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
//$mail->Body    = '$texto. This is the HTML message body <b>in bold!</b>';
//$mail->Body = emailEESC($texto);
//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
//$mail->AddEmbeddedImage($file, 'cartao', $file);
//$mail->AddEmbeddedImage($texto, 'cartao', $texto);

//$mail->MsgHTML('<img src="cid:cartao">');
$mail->MsgHTML($texto);

$mail->Send();
      if(!$mail->Send()) {
           echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
          echo "";
       }

//---- fim

         $query = new dbSqlConnectionBIBLIOTECA();
	 $query->query("SELECT id_grupo FROM mala_direta_grupos WHERE grupo = 'Semana De Pos-Graduacao'");
	 if ($query->next_record())
	 {
		$grupo = $query->f('id_grupo');
	 }
	 else
	 {
		// $query->query("INSERT INTO mala_direta_grupos (grupo) VALUES('Semana De Pos-Graduacao')");
		 $query->query("SELECT id_grupo FROM mala_direta_grupos WHERE grupo = 'Semana De Pos-Graduacao'");
		 $query->next_record();
		 $grupo = $query->f('id_grupo');
	 }


	 $query->query("SELECT id_email FROM mala_direta_emails WHERE email = '$email'");
	 if ($query->next_record())
         {
		$email = $query->f('id_email');
		$query->query("SELECT COUNT(*) as total FROM mala_direta_email_grupo WHERE id_grupo = '$grupo' AND id_email = '$email'");
		$query->next_record();
		if (!$query->f('total'))
			$query->query("INSERT INTO mala_direta_email_grupo VALUES ($grupo, $email)");
         }
 	 else
 	 {
		$query->query("INSERT INTO mala_direta_emails (email) VALUES ('$email')");
		$query->query("SELECT id_email FROM mala_direta_emails WHERE email = '$email'");
		$query->next_record();
		$email = $query->f('id_email');
		$query->query("SELECT COUNT(*) as total FROM mala_direta_email_grupo WHERE id_grupo = $grupo AND id_email = $email");
		$query->next_record();
		if (!$query->f('total'))
			$query->query("INSERT INTO mala_direta_email_grupo VALUES ($grupo, $email)");
	 }

	$tpl->parse("bENVIADO", "block_ENVIADO", true);
	$inseri = 1;
   }
}

if (!$inseri)
{
	$tpl->parse("bFORMULARIO", "block_FORMULARIO", true);

	$query = new dbSqlConnectionBIBLIOTECA();
	$query->query("SELECT p.id_prog, p.titulo, p.nrovagas, COUNT(i.id_inscrito) as inscritos, p.data data FROM semanapos_prog_n2021 p LEFT JOIN semanapos_inscricoes_n i ON (i.id_prog = p.id_prog) AND (i.ano = 2021) WHERE disponivel = 1 GROUP BY p.id_prog ORDER BY data ASC, horainicio ASC");

	while ($query->next_record())
	{
		list($ano, $mes, $dia) = sscanf($query->f('data'),"%04d-%02d-%02d");
		$data_palestra = mktime(0,0,0,$mes,$dia,$ano);
		$hoje = mktime(0,0,0,date("m"),date("d"),date("Y"));
//		if ($data_palestra >= $hoje)
		if ($data_palestra >= $hoje)
		{
			$checked = in_array($query->f('id_prog'),$id_progs) ? "checked" : null;
//			$checkbox = $query->f('inscritos') >= $query->f('nrovagas') ? "<i>(esgotado)</i>" :  "<input type=\"checkbox\" name=\"id_progs[]\" value=\"".$query->f('id_prog')."\" $checked  />";
			$checkbox = $query->f('inscritos') >= $query->f('nrovagas') ? "<i>(confirmar vaga no local)</i>" :  "<input type=\"checkbox\" name=\"id_progs[]\" value=\"".$query->f('id_prog')."\" $checked  />";

		}
		else $checkbox = "<i>(encerrado)</i>";

		$tpl->set_var("CHECKBOX", $checkbox);
		$tpl->set_var("ID_PROG",$query->f('id_prog'));
		$tpl->set_var("TITULO", $query->f('titulo'));
		$tpl->parse("bPROGS","block_PROGS",true);
	}

	if ($inscricoesencerradas)
		$tpl->parse("bINSCRICOESENCERRADAS","block_INSCRICOESENCERRADAS",true);

	$tpl->set_var("NOME", $nome);
	$tpl->set_var("EMAIL", $email);
	$tpl->set_var("CONF_EMAIL", $conf_email);
	$tpl->set_var("CPF", $cpf);
	$tpl->set_var("TELEFONE", $telefone);
	$tpl->set_var("CIDADE", $cidade);
	$tpl->set_var("PROFISSAO", $profissao);

	if (is_array($erros))
	{
	        $tpl->set_var("TEXTO", implode('<br>',$erros));
	        $tpl->parse("bERROS","block_ERROS",true);
	}
}

$tpl->parse("final","main");
$tpl->p("final");

?>
