<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

 <head>
  <title>XVII Semana da Pós-Graduação na EESC e II Semana da Pós-Graduação do ICMC</title>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta http-equiv="Content-Language" content="pt_br">
  <meta http-equiv="Description" content="Escola de Engenharia de São Carlos da Universidade de São Paulo - USP">
  <meta http-equiv="Keywords" content="Escola de Engenharia de São Carlos, EESC, Universidade de São Paulo, USP, São Carlos, Engenharia, Mecânica, Elétrica, Hidráulica, Civil, Produção, Tecnologia, Pesquisa, Inovação, Graduação, Pós-Graduação, Cultura, Alunos, Universidade, Pós-Graduação, Biblioteca">
  <link rel="stylesheet" type="text/css" href="includes/styles.css" />
  <link rel="stylesheet" type="text/css" href="chrometheme/chromestyle.css" />
  <script type="text/javascript" src="chromejs/chrome.js"></script>
 </head>

 <body>
  <div id="general">
   <div id="bar"></div>
   <div id="header"></div>
   <div class="chromestyle" id="chromemenu">
    <ul>
     <li><a href="./index.php">Home</a></li>
     <li><a href="./programacao.php">Programa&ccedil;&atilde;o</a></li>
<!--
     <li><a href="#" rel="dropmenu1" >Inscri&ccedil;&atilde;o</a></li>
-->
     <li><a href="./inscricoes.php">Inscri&ccedil;&atilde;o</a></li> 
     <li><a href="./infos_gerais.php">Informa&ccedil;&otilde;es Gerais</a></li>
     <li><a href="./downloads.php">Downloads</a></li>
     <li><a href="./links_gerais.php">Links</a></li>
     <li><a href="#" rel="dropmenu2">Edi&ccedil;&otilde;es Anteriores</a></li>
     <li><a href="./contato.php">Contato</a></li>
    </ul>
   </div>
                                                  
<!--
   <div id="dropmenu1" class="dropmenudiv">
    <a href="./inscricoes.php">Presencial</a>
    <a href="./inscricoesvc.php">Videoconfer&ecirc;ncia</a>
   </div>                                                  
-->

   <div id="dropmenu2" class="dropmenudiv" style="width: 155px;">
    <a href="http://sistemas.eesc.usp.br/semana_pos2017" target="_blank">2017</a>
    <a href="http://sistemas.eesc.usp.br/semana_pos2018" target="_blank">2018</a>
    <a href="http://sistemas.eesc.usp.br/semana_pos2019" target="_blank">2019</a>
    <a href="http://sistemas.eesc.usp.br/semana_pos2020" target="_blank">2020</a>
   </div>
   <script type="text/javascript">
    cssdropdown.startchrome("chromemenu")
   </script>
   <div id="content">
   {CERTIFICADOS}
   {PAGINA}<p>&nbsp;</p>
   </div>
   <div id="realizacao">
    <b>Realização:</b><br><br>
    <a href="http://www.eesc.usp.br/" target="_blank"><img src="./images/logo_eesc_vertical1050.jpg" border="0"></a>&nbsp;|&nbsp;
    <a href="http://www.eesc.usp.br/biblioteca" target="_blank"><img src="./images/SBINovo_d.png" border="0"></a>&nbsp;|&nbsp;
    <a href="http://www.eesc.usp.br/institucional/colegiados.php" target="_blank"><img src="./images/CPG3.png" border="0"></a>&nbsp;|&nbsp;
    <a href="http://www.icmc.usp.br/pos-graduacao" target="_blank"><img src="./images/logo50_icmc_50anos.jpg" border="0"></a>&nbsp;&nbsp;
   </div> 
<!--   <div id="apoio">
    <b>Patrocínio:</b><br><br>
    <p align="center"><img src="images/incentivo/dotlib_b.jpg" border="0"></p>
   </div> -->
   <!--
   <div id="apoio">
	<b>Patrocínio:</b><br><br>
                <p align="center">
                    <img src="images/incentivo/pat1.gif" width="250px">
                    <img src="images/incentivo/Logo_SmartPLM.jpg" width="180px">
                </p>
   </div>
   -->
   <div id="apoio">
    <b>Apoio:</b><br><br>
    <p align="center">{A1}</p>
   </div>
   <div id="footer">
    XVII Semana da Pós-Graduação na EESC<br>e II Semana da Pós-Graduação do ICMC - USP<br>Seção Técnica de Informática
   </div>
  </div>
 </body>

</html>
