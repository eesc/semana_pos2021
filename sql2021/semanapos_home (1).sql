-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Abr 29, 2021 as 11:02 AM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_home`
--

CREATE TABLE IF NOT EXISTS `semanapos_home` (
  `home` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ano`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_home`
--

INSERT INTO `semanapos_home` (`home`, `ano`, `ip`, `data_hora`) VALUES
('<p style="text-align: center;"><strong><span style="color: #ff0000;"><span style="font-size: x-small;"><span style="font-size: medium;">ATEN&Ccedil;&Atilde;O</span></span></span></strong></p>\r\n<p style="text-align: center;"><span><span style="color: #000000;"><strong>Alguns&nbsp;Certificados n&atilde;o puderam ser enviados.<br /></strong></span><span style="color: #000000;"><strong>Antes de solicitar novamente&nbsp;(em</strong> </span></span><span style="color: #0000ff;"><span style="color: #0000ff;"><span style="color: #000000;"><strong>contato</strong></span></span></span><span style="color: #ff6600;"><span style="color: #000000;"><strong>), <br />favor esvaziar a caixa de entrada de seu Email.</strong></span></span></p>\r\n<p style="text-align: center;"><span style="color: #ff6600;"><span style="color: #000000;"><strong>OBS: Os Certificados do "TREINAMENTO: <span style="color: #000000;">Portal Peri&oacute;dicos CAPES - Base de Dados"<br /> ser&atilde;o enviados pela CAPES e n&atilde;o pela Comiss&atilde;o Organizadora da IV Semana.</span></strong></span></span></p>\r\n<p style="text-align: center;"><strong></strong></p>\r\n<p style="text-align: center;"><strong><span style="color: #000000;">OBJETIVO</span></strong><br /><br />Reunir a comunidade cient&iacute;&shy;fica local e profissionais de &aacute;reas relacionadas &agrave; pesquisa, atrav&eacute;s de atividades que incluem palestras com temas espec&iacute;ficos, mini-cursos e workshops.<br /><br /><strong></strong><br /><strong>P&Uacute;BLICO ALVO</strong></p>\r\n<p style="text-align: center;">Alunos de p&oacute;s-gradua&ccedil;&atilde;o, gradua&ccedil;&atilde;o, docentes, pesquisadores e interessados em informa&ccedil;&atilde;o t&eacute;cnico-cient&iacute;fica<br />nas diversas &aacute;reas do conhecimento.<br /><br /><br /><strong></strong>&nbsp;<strong>CERTIFICADOS</strong></p>\r\n<p style="text-align: center;"><span style="color: #ff6600;"><span style="color: #000000;">A entrega do certificado de participa&ccedil;&atilde;o foi feita atrav&eacute;s do e-mail indicado no ato da inscri&ccedil;&atilde;o.</span></span><br /><br /><br /><br /><strong>COMISS&Atilde;O ORGANIZADORA</strong><br /><br />COORDENA&Ccedil;&Atilde;O<br /><br />Geraldo Roberto Martins da Costa<br />Teresinha das Gra&ccedil;as Coletta<br /><br />ORGANIZA&Ccedil;&Atilde;O<br /><br />Adriana Coscia Perez Gomes<br />Ana Carolina Venere Murata<br />Elena Luzia Palloni Gon&ccedil;alves<br />Elenise Maria de Ara&uacute;jo<br />Gertrud Isabel B. Romanelli<br />Jo&atilde;o Francisco Labela<br />Lucia Semensato Zanetti<br />Marielza Ortega Roma<br />Murillo Ferreira de Camargo<br />Rosana Alvarez Paschoalino<br />Roberto Jos&eacute; Mazzari<br />Sabrina Dotta de Brito<br />Vera Lucia Lioni Pedrini</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style="padding-left: 60px;">&nbsp;</p>\r\n<p style="padding-left: 60px;"><span style="text-decoration: underline;"><strong><span style="color: #ff6600;">A&Ccedil;&Atilde;O SOCIAL</span></strong></span></p>\r\n<p style="padding-left: 60px;"><strong><span style="color: #ff6600;">Durante o evento receberemos doa&ccedil;&atilde;o de leite longa vida em prol da "Casa da Crian&ccedil;a</span><span style="color: #ff6600;"> de S&atilde;o Carlos"<br />Fa&ccedil;a sua contribui&ccedil;&atilde;o!</span></strong></p>\r\n<p style="padding-left: 60px;"><span style="color: #ff6600;"><span style="color: #ff6600;"><span style="color: #ff6600;"><strong></strong></span></span></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;">&nbsp;</p>', 2008, '143.107.227.80', '2008-05-15 10:28:18');
