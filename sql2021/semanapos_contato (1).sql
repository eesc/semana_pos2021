-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Abr 29, 2021 as 11:00 AM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_contato`
--

CREATE TABLE IF NOT EXISTS `semanapos_contato` (
  `contato` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `texto` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_contato`
--

INSERT INTO `semanapos_contato` (`contato`, `ano`, `ip`, `data_hora`, `texto`) VALUES
('semana@eesc.usp.br', 2008, '143.107.227.110', '2008-10-02 11:12:56', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da IV Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha<br />os campos abaixo que em breve receber&aacute; retorno.</p>');
