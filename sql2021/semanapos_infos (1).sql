-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Abr 29, 2021 as 11:03 AM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_infos`
--

CREATE TABLE IF NOT EXISTS `semanapos_infos` (
  `infos` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ano`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_infos`
--

INSERT INTO `semanapos_infos` (`infos`, `ano`, `ip`, `data_hora`) VALUES
('<p>&nbsp;</p>\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Verdana;">Endereços e mapas:</span></strong><span style="font-size: 10pt; font-family: Verdana;"></span></p>\r\n<p style="text-align: center;" align="center"><span style="font-size: 10pt; font-family: Verdana;"><a href="http://www.eesc.usp.br/institucional/enderecos-mapas.php">http://www.eesc.usp.br/institucional/enderecos-mapas.php</a></span></p>\r\n<p style="text-align: center;" align="center"><strong><span style="font-size: 10pt; font-family: Verdana;">Mais Informa&ccedil;&otilde;es:</span></strong><span style="font-size: 10pt; font-family: Verdana;"></span></p>\r\n<p style="text-align: center;" align="center"><span style="font-size: 10pt; font-family: Verdana;"><a href="https://www.visitesaocarlos.com/" target="_blank"><span style="color: #0000ff;">https://www.visitesaocarlos.com/</span></a></span></p>\r\n<p>&nbsp;</p>', 2008, '143.107.227.110', '2008-09-29 16:00:27');
