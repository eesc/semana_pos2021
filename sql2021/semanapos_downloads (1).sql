-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Abr 29, 2021 as 11:01 AM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_downloads`
--

CREATE TABLE IF NOT EXISTS `semanapos_downloads` (
  `id_download` int(11) NOT NULL AUTO_INCREMENT,
  `download` text NOT NULL,
  `tamanho` text NOT NULL,
  `descricao` text NOT NULL,
  `ano` int(4) NOT NULL,
  `downloads` int(11) NOT NULL DEFAULT '1',
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` text NOT NULL,
  PRIMARY KEY (`id_download`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Extraindo dados da tabela `semanapos_downloads`
--

INSERT INTO `semanapos_downloads` (`id_download`, `download`, `tamanho`, `descricao`, `ano`, `downloads`, `data_hora`, `ip`) VALUES
(36, 'Marcio Fabricio.pdf', '264031', '23/abr - mesa redonda - marcio fabricio', 2008, 1, '2008-04-24 12:13:39', '143.107.227.80'),
(43, 'Lilia Marmorato.pdf', '28231', '23/abr - redacao de textos academicos - traducao', 2008, 1, '2008-04-28 08:44:54', '143.107.227.80'),
(35, 'Conduta e Equilibrio.pdf', '1857185', '23/abr - conduta e equilibrio emocional', 2008, 1, '2008-04-24 12:13:00', '143.107.227.80'),
(31, 'Elsevier - 02.pdf', '1638387', '22/abr - elsevier - 02', 2008, 1, '2008-04-24 12:02:19', '143.107.227.80'),
(32, 'Elsevier - 01.pdf', '2525724', '22/abr - elsevier - 01', 2008, 1, '2008-04-24 12:02:38', '143.107.227.80'),
(33, 'fator de impacto e indice h.pdf', '2698139', '22/abr - fator de impacto e &iacute;ndice h', 2008, 1, '2008-04-24 12:06:10', '143.107.227.80'),
(25, 'Web of Science (Thomson) - 01.pdf', '224843', '22/abr - web of science (thomson) - 01', 2008, 1, '2008-04-24 12:00:06', '143.107.227.80'),
(22, 'Web of Science (Thomson) - 02.pdf', '2512917', '22/abr - web of science (thomson) - 02', 2008, 1, '2008-04-24 11:50:01', '143.107.227.80'),
(20, 'Apresentacao do Portal de Periodicos da Capes.pdf', '6528746', '22/abr - treinamento: portal peri&oacute;dicos capes', 2008, 1, '2008-04-24 15:06:57', '143.107.227.80'),
(37, 'Citacoes.pdf', '571417', '23/abr - mini-curso: cita&ccedil;&otilde;es', 2008, 1, '2008-04-24 15:05:09', '143.107.227.80'),
(38, 'Referencias.pdf', '644382', '23/04 - mini-curso: refer&ecirc;ncias', 2008, 1, '2008-04-24 15:05:35', '143.107.227.80'),
(39, 'Elaboracao de Resumos.pdf', '3690144', '24/abr - mini-curso: elabora&ccedil;&atilde;o de resumos', 2008, 1, '2008-04-24 15:06:22', '143.107.227.80'),
(40, 'Elias Hage.pdf', '547838', '23/abr - mesa redonda - elias hage', 2008, 1, '2008-04-24 14:25:37', '143.107.227.80'),
(41, 'EESC_25_04_08.ppt', '1456640', '25/abr - a p&oacute;s-gradua&ccedil;&atilde;o na usp e sua internacionaliza&ccedil;&atilde;o', 2008, 1, '2008-04-28 12:08:03', '143.107.227.80'),
(50, 'teses digitais 2008 - submissao.pdf', '838344', '25/abr - teses digitais - submiss&atilde;o', 2008, 1, '2008-04-29 10:42:12', '143.107.227.80'),
(49, 'teses digitais 2008 - semana da pos.pdf', '3088627', '25/abr - teses digitais', 2008, 1, '2008-04-28 12:06:31', '143.107.227.80'),
(51, 'Apresentacoes Eficientes - Fagner Franca.pdf', '6181230', '25/abr - slides para apresenta&ccedil;&otilde;es eficientes', 2008, 1, '2008-05-21 08:29:47', '143.107.227.110');
