-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Abr 29, 2021 as 11:02 AM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_home_n`
--

CREATE TABLE IF NOT EXISTS `semanapos_home_n` (
  `home` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ano`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_home_n`
--

INSERT INTO `semanapos_home_n` (`home`, `ano`, `ip`, `data_hora`) VALUES
('<address>\r\n<h5 style="text-align: center; "><span style="font-family: Arial; color: #0000ee; font-size: xx-small;"><span style="font-style: normal; font-weight: 400; white-space: pre-wrap;"><span style="text-decoration: underline;"><a href="http://www.jaguar.eesc.usp.br/eesc/administracao/biblioteca/pub/semana_pos2021/sticursos/estagiarios/" target="_blank"><span style="font-size: small;">Acesse aqui os certificados da XVII Semana da P&oacute;s-Gradua&ccedil;&atilde;o na EESC e II Semana da P&oacute;s-Gradua&ccedil;&atilde;o do ICMC</span></a></span></span></span></h5>\r\n<h5 style="text-align: center; "><span style="font-style: normal; font-weight: normal;"><span style="font-size: x-small;"><span style="font-size: small;">Este evento tem como objetivo integrar os alunos ingressantes nos programas de p&oacute;s-gradua&ccedil;&atilde;o oferecidos pela Escola de Engenharia de S&atilde;o Carlos e pelo Instituto de Ci&ecirc;ncias Matem&aacute;ticas e de Computa&ccedil;&atilde;o &agrave; comunidade acad&ecirc;mica do campus USP- S&atilde;o Carlos.</span></span></span></h5>\r\n<h5 style="text-align: center; "><span style="font-style: normal; font-weight: normal;"><span style="font-size: x-small;"><span style="font-size: small;">O evento &eacute; gratuito e aberto para todos os alunos e interessados em conhecer um pouco mais sobre a P&oacute;s-Gradua&ccedil;&atilde;o da USP. Foram convidados palestrantes e especialistas em temas relevantes para as atividades da pesquisa acad&ecirc;mica e bem estar. Observe a programa&ccedil;&atilde;o e escolha as palestras, treinamentos, worshops que foram especialmente escolhidos para atender as demandas dos alunos da pesquisa na p&oacute;s-gradua&ccedil;&atilde;o.</span></span></span></h5><span style="font-style: normal;"><span style="font-size: x-small;"><span style="font-size: small;"> </span>\r\n<div style="text-align: center;"><span style="font-size: small;">O evento acontece h&aacute; 16 anos e nesta edi&ccedil;&atilde;o de 2021 todas as atividades ser&atilde;o oferecidas remotamente a partir de links em salas virtuais e com transmiss&atilde;o pelo canal youtube da Biblioteca da EESC e do ICMC.</span></div>\r\n<div style="text-align: center;"><span style="font-size: medium;"><span style="font-style: normal;">&nbsp;&nbsp;</span></span></div>\r\n<div style="text-align: center;"><strong><a href="http://icmc.usp.br/e/b2fd8" target="_blank">DEIXE AQUI SUA&nbsp;AVALIA&Ccedil;&Atilde;O DO EVENTO</a></strong></div> </span></span></address>', 2021, '189.103.16.138', '2021-04-14 17:40:58');
