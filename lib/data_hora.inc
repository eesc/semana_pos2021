<?php

/************************************************************************************
 Fun��o que retorna um "datetime" tomando como par�metro uma data em um dos formato:
 "dd/mm/aaaa", "dd-mm-aaaa", "dd.mm-aaaa".
 ************************************************************************************/
function data_datetime($data_in)
{
   if (! preg_match("/([0-9]{1,2})[\/.-]([0-9]{1,2})[\/.-]([0-9]{4})/", $data_in, $regs)) 
       $data_out = 0;
   elseif (! checkdate($regs[2],$regs[1],$regs[3]))
	   $data_out = 0;
   else
       $data_out = mktime(0,0,0,$regs[2],$regs[1],$regs[3]);
   return $data_out;
}

/************************************************************************************
 Fun��o que retorna uma data no formato "aaaa-mm-dd" aceito pelo MySql tomando como 
 par�metro uma data em um dos formato:
 "dd/mm/aaaa", "dd-mm-aaaa", "dd.mm-aaaa".
 ************************************************************************************/
function data_mydate($data_in)
{
   if (! preg_match("/([0-9]{1,2})[\/.-]([0-9]{1,2})[\/.-]([0-9]{4})/", $data_in, $regs))
      $data_out = '';
   elseif(strlen(trim($data_in)) != strlen($regs[1] . '/' . $regs[2] . '/' . $regs[3]))
      $data_out = '';
   elseif (! checkdate($regs[2],$regs[1],$regs[3]))
	  $data_out = '';
   else
      $data_out = $regs[3] . '-' . $regs[2] . '-' . $regs[1];
   return $data_out;
}

/************************************************************************************
 Fun��o que retorna uma data no formato "dd/mm/aaaa" tomando como par�metro uma data 
 no formato "aaaa-mm-dd" do MySql
 ************************************************************************************/
function mydate_data($data_in)
{
   if (!  preg_match("/([0-9]{4})[\/.-]([0-9]{1,2})[\/.-]([0-9]{1,2})/", $data_in, $regs)) 
      $data_out = '';
   elseif (! checkdate($regs[2],$regs[3],$regs[1]))
      $data_out = '';
   else
      $data_out = $regs[3] . '/' . $regs[2] . '/' . $regs[1];
   return $data_out;
}

/************************************************************************************
 Transforma datas no formato "1/10" ou "1.10.02" ou "1-10-2002" em "1/10/2002"
 ************************************************************************************/
function data_DD_MM_AAAA($data, $ano_default = '')
{
   $data = trim($data);
   if ($ano_default <> '')
      if (! eregi ('([0-9]{4})', $ano_default))
	     return 0;
   if (! eregi ('([0-9]{1,2}[/.-][0-9]{1,2}[/.-][0-9]{4})', $data))
      if (! eregi ('([0-9]{1,2}[/.-][0-9]{1,2}[/.-][0-9]{2})', $data))
         if (! eregi ('([0-9]{1,2}[/.-][0-9]{1,2})', $data))
            return 0;
   $a_campos = spliti('[/-]',$data);

   if (strlen($a_campos[2])==0 && count($a_campos)==2)
   {  
      if ($ano_default == '')
         $a_campos[2] = date("Y");
	  else
         $a_campos[2] = $ano_default;
   }
   else if (strlen($a_campos[2])==2)
      $a_campos[2] = substr(date("Y"),0,2) . $a_campos[2];
   else if (strlen($a_campos[2])!=4)
      return 0;

   $data_out = data_datetime("$a_campos[0]/$a_campos[1]/$a_campos[2]");
   if ($data_out)
      return date('d/m/Y',$data_out);
   else
      return 0;
}

/************************************************************************************
 Separa datas no formato "1-10-02 a 4.10.2002" ou "1/10 a 4/10" em um array no formato
 "01/10/2002 02/10/2002 03/10/2002 04/10/2002"
 ************************************************************************************/
function separar_datas_intervalo($intervalo, $ano_default = '')
{
   $a_intervalo = array();
   $a_datas = array();

   // pega a data inicial e a final
   $a_datas = spliti('[a]',$intervalo);

   $sDataIni = data_DD_MM_AAAA($a_datas[0], $ano_default);
   if (!$sDataIni) 
      return 0;
   $sDataFim = data_DD_MM_AAAA($a_datas[1], $ano_default);
   if (!$sDataFim) 
      return 0;
   
   // monta o intervalo
   $iDataIni = data_datetime($sDataIni);
   $iDataFim = data_datetime($sDataFim);
   for ($i = $iDataIni; $i<=$iDataFim; $i+=86400)  // 1 dia = 86400 segundos
      array_push($a_intervalo,date("d/m/Y",$i));
		 
   // verifica se conseguiu separar as dadas	 
   if (count($a_intervalo)==0)
      return 0;

   return $a_intervalo;
}

/************************************************************************************
 Separa datas no formato "1/10 a 5/10, 14/10, 15.10.02 e 21-11-2002" em um array
 ************************************************************************************/
function separar_datas($datas_in, $ano_default = '')
{
   $a_datas_digitadas = array();
   $a_datas_intervalo = array();
   $a_datas_out = array();
 
   $a_datas_digitadas = spliti('[,e]',$datas_in);
   
   foreach ($a_datas_digitadas as $d)
   {
      $dt = trim($d);
    
      if (eregi('a',$dt))
	  {
	     if ($a_datas_intervalo = separar_datas_intervalo($d))
		    if (!$a_datas_intervalo)
			   return 0;
			else
               foreach ($a_datas_intervalo as $di)
	              array_push($a_datas_out,$di);
		 else
		    return 0;
	  }
	  else
	  {
	     $dt = data_DD_MM_AAAA($dt, $ano_default);
		 
	     if (!$dt)
	        return 0;
         else
	        array_push($a_datas_out,$dt);
      }
   }

   return $a_datas_out;
}

/************************************************************************************
 Separa datas no formato "1-10-02 a 4.10.2002" ou "1/10 a 4/10" em uma string no formato
 "01/10/2002,04/10/2002"
 ************************************************************************************/
function separar_datas_intervalo_ini_fim($intervalo, $ano_default = '')
{
   // pega a data inicial e a final
   $a_datas = spliti('[a]',$intervalo);

   $sDataIni = data_DD_MM_AAAA($a_datas[0], $ano_default);
   if (!$sDataIni) 
      return 0;
   $sDataFim = data_DD_MM_AAAA($a_datas[1], $ano_default);
   if (!$sDataFim) 
      return 0;
   if (data_datetime($sDataIni) > data_datetime($sDataFim))
      return 0;
   return "$sDataIni,$sDataFim";
}

/************************************************************************************
 Separa datas no formato "1-10-02 a 4.10.2002, 1/10 e 4/10" em um array no formato
 "01/10/2002,04/10/2002 01/10/2002,01/10/2002 04/10/2002,04/10/2002"
 ************************************************************************************/
function separar_datas_ini_fim($datas_in, $ano_default = '')
{
   $a_datas_digitadas = array();
   $a_datas_intervalo = array();
   $a_datas_out = array();
 
   $a_datas_digitadas = spliti('[,e]',$datas_in);
   
   foreach ($a_datas_digitadas as $d)
   {
      $dt = trim($d);
    
      if (eregi('a',$dt))
	  {
	     $data = separar_datas_intervalo_ini_fim($dt);
		 if ($data)
            array_push($a_datas_out,$data);
	     else
		    return 0;
	  }
	  else
	  {
	     $dt = data_DD_MM_AAAA($dt, $ano_default);
		 
	     if (!$dt)
	        return 0;
         else
	        array_push($a_datas_out,"$dt,$dt");
      }
   }

   return $a_datas_out;
}

/************************************************************************************
 Fun��o que retorna uma hora no formato "HH:MM" tomando como par�metro uma hora
 no formato "HH:MM:SS" do MySql
 ************************************************************************************/
function mytime_hora($hora_in)
{
   if (! preg_match ("/([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})/", $hora_in, $regs)) 
      $hora_out = '';
   else 
      $hora_out = $regs[1] . ':' . $regs[2];
   return $hora_out;
}

/************************************************************************************
 Separa horas no formato "8:00 as 10:00, 10:00 as 12:00 e 14:00 as 16:00" em um array
 no formato "8:00,10:00 10:00,12:00 14:00,16:00"
 ************************************************************************************/
function separar_horas_ini_fim($horas_in)
{
   $a_horas_out = array();

   $a_horas_digitadas = spliti('[,e]',$horas_in);
   foreach($a_horas_digitadas as $intervalo)
   {
      $intervalo = trim($intervalo);
	  if (!eregi("([0-9]{1,2}:[0-9]{1,2})[ ]*as[ ]*([0-9]{1,2}:[0-9]{1,2})",$intervalo))
	     return 0;
	  $horas = spliti('as',$intervalo);
	  $hs = trim($horas[0]) . ',' . trim($horas[1]);
      array_push($a_horas_out,$hs);
   }
   return $a_horas_out;
}

?>
