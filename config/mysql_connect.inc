<?php

include_once("config.inc.php");
include_once("db_mysql.inc");

class dbSqlConnection extends DB_Sql
{
   function dbSqlConnection($query = "")
   {
     global $dbHost,$dbUser,$dbPassword,$dbDatabase;
	
     if ($query=="")
        $this->connect($dbHost,$dbUser,$dbPassword,$dbDatabase);
     else
        $this->query($query);
   }
}

class dbSqlConnection1 extends DB_Sql
{
   function dbSqlConnection1($query = "")
   {
     global $dbHost,$dbUser,$dbPassword,$dbDatabase1;

     if ($query=="")
        $this->connect($dbHost,$dbUser,$dbPassword,$dbDatabase1);
     else
        $this->query($query);
   }
}

class dbSqlConnection2 extends DB_Sql
{
   function dbSqlConnection2($query = "")
   {
     global $dbDatabase2,$dbHost,$dbUser,$dbPassword;

     if ($query=="")
        $this->connect($dbDatabase2,$dbHost,$dbUser,$dbPassword);
     else
        $this->query($query);
   }
}

class dbSqlConnection3 extends DB_Sql
{
   function dbSqlConnection3($query = "")
   {
     global $dbDatabase3,$dbHost,$dbUser,$dbPassword;

     if ($query=="")
        $this->connect($dbDatabase3,$dbHost,$dbUser,$dbPassword);
     else
        $this->query($query);
   }
}




class dbSqlConnection4 extends DB_Sql
{
   function dbSqlConnection4($query = "")
   {
     global $dbDatabase4,$dbHost4,$dbUser4,$dbPassword4;

     if ($query=="")
        $this->connect($dbDatabase4,$dbHost4,$dbUser4,$dbPassword4);
     else
        $this->query($query);
   }
}









class dbSqlConnectionBIBLIOTECA extends DB_Sql
{
   function dbSqlConnectionBIBLIOTECA($query = "")
   {
     global $dbHost,$dbUserBIBLIOTECA,$dbPasswordBIBLIOTECA,$dbDatabaseBIBLIOTECA;

     if ($query=="")
        $this->connect($dbHost,$dbUserBIBLIOTECA,$dbPasswordBIBLIOTECA,$dbDatabaseBIBLIOTECA);
     else
        $this->query($query);
   }
}


class dbSqlConnectionPUMA extends DB_Sql
{
   function dbSqlConnectionPUMA($query = "")
   {
     global $dbDatabasePUMA,$dbHostPUMA,$dbUserPUMA,$dbPasswordPUMA;

     if ($query=="")
        $this->connect($dbDatabasePUMA,$dbHostPUMA,$dbUserPUMA,$dbPasswordPUMA);
     else
        $this->query($query);
   }
}

?>
