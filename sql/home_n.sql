-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 05:02 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_home_n`
--

CREATE TABLE IF NOT EXISTS `semanapos_home_n` (
  `home` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ano`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_home_n`
--

INSERT INTO `semanapos_home_n` (`home`, `ano`, `ip`, `data_hora`) VALUES
('<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><span style="color: #0000ff; font-size: x-small;"><a href="http://www.eesc.usp.br/eesc/administracao/biblioteca/pub/semana_pos2020/sticursos/estagiarios/" target="_blank"><span style="font-size: small;"><span style="color: #000080; font-size: medium;">Acesse aqui os certificados de participa&ccedil;&atilde;o&nbsp;</span></span></a></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><span style="font-size: medium;"><a href="https://photos.app.goo.gl/tMZUKxM9UJQxt4jX8" target="_blank">Acesse aqui as fotos do evento&nbsp;</a></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><span style="font-size: small;">&nbsp;</span><strong>XVI Semana da P&oacute;s-Gradua&ccedil;&atilde;o na EESC</strong></p>\r\n<p class="MsoNormal" style="text-align: center;">11 a 15 de mar&ccedil;o de 2020</p>\r\n<p class="MsoNormal" style="text-align: center;">&nbsp;</p>\r\n<p class="MsoNormal" style="text-align: center;">Promovida pela Comiss&atilde;o de P&oacute;s-Gradua&ccedil;&atilde;o e pela Biblioteca\r\nda Escola de Engenharia de S&atilde;o Carlos (EESC-USP), a XVI Semana da P&oacute;s-Gradua&ccedil;&atilde;o\r\nna EESC re&uacute;ne in&uacute;meras atividades de apoio a pesquisa acad&ecirc;mica dos alunos e\r\ndocentes dos programas de p&oacute;s-gradua&ccedil;&atilde;o, bibliotec&aacute;rios, educadores e demais\r\ninteressados da Escola, do campus e da comunidade externa.</p>\r\n<p class="MsoNormal" style="text-align: center;">Ser&atilde;o realizadas palestras, mesas redondas, workshops,\r\noficinas que abordar&atilde;o os seguintes temas: boas pr&aacute;ticas e &eacute;tica na pesquisa,\r\nparcerias entre a universidade e setor privado, processo de inova&ccedil;&atilde;o e produ&ccedil;&atilde;o\r\nde patentes, gest&atilde;o do tempo de pesquisa, t&eacute;cnicas de reda&ccedil;&atilde;o, apresenta&ccedil;&atilde;o em\r\np&uacute;blico e dicas para o bem estar f&iacute;sico e emocional do aluno.</p>\r\n<p class="MsoNormal" style="text-align: center;">&nbsp;</p>\r\n<p class="MsoNormal" style="text-align: center;"><span style="font-weight: bold;">Sorteios</span></p>\r\n<p class="MsoNormal" style="text-align: center;">Todos os dias haver&aacute; sorteio de brindes todos oferecidos\r\npelas empresas apoiadoras.</p>\r\n<p class="MsoNormal" style="text-align: center;">&nbsp;</p>\r\n<p class="MsoNormal" style="text-align: center;">&nbsp;<span style="font-weight: 700;">A&ccedil;&atilde;o Social</span>&nbsp;</p>', 2020, '143.107.227.79', '2019-03-29 16:39:44');
