-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 05:07 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_prog`
--

CREATE TABLE IF NOT EXISTS `semanapos_prog` (
  `id_prog` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `resumo` text NOT NULL,
  `palestrante` text NOT NULL,
  `data` date NOT NULL DEFAULT '0000-00-00',
  `horainicio` time NOT NULL DEFAULT '00:00:00',
  `horafim` time NOT NULL DEFAULT '00:00:00',
  `nrovagas` smallint(6) NOT NULL DEFAULT '0',
  `disponivel` char(1) NOT NULL DEFAULT '1',
  `local` text NOT NULL,
  `ano` int(4) NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_prog`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `semanapos_prog`
--

INSERT INTO `semanapos_prog` (`id_prog`, `titulo`, `resumo`, `palestrante`, `data`, `horainicio`, `horafim`, `nrovagas`, `disponivel`, `local`, `ano`, `data_hora`, `ip`) VALUES
(1, 'MINI-CURSO: Citações e Referências', '<p>Tem como objetivo orientar os participantes na elabora&ccedil;&atilde;o das refer&ecirc;ncias e cita&ccedil;&otilde;es de acordo com as normas NBR 6023/2002 e NBR 10520/2002 respectivamente, nos trabalhos acad&ecirc;micos.</p>', 'Elena Luzia Palloni Gonçalves (Bibliotecária EESC)', '2008-04-23', '19:00:00', '22:00:00', 100, '1', 'Sala de aula da Biblioteca', 2008, '2008-04-22 11:42:40', '143.107.227.80'),
(4, 'MINI-CURSO: Elaboração de Resumos', '<p>Levar ao conhecimento dos p&oacute;s-graduandos os recursos informacionais dispon&iacute;veis e tamb&eacute;m orienta&ccedil;&otilde;es normativas para reda&ccedil;&atilde;o de resumos de trabalhos acad&ecirc;micos com o objetivo de aumentar o interesse pela qualidade do tema.</p>', 'Rosana Alvarez Paschoalino (Bibliotecária EESC)', '2008-04-24', '09:00:00', '12:00:00', 100, '1', 'Sala de aula da Biblioteca', 2008, '2008-04-22 11:42:51', '143.107.227.80'),
(5, 'PALESTRA: Redação de Textos Acadêmicos', '<p></p>\r\n<p>Exposi&ccedil;&atilde;o e discuss&atilde;o das melhores pr&aacute;ticas na elabora&ccedil;&atilde;o de textos, artigos, disserta&ccedil;&otilde;es e teses com base em textos na l&iacute;ngua portuguesa e inglesa.</p>', 'Lilia Maria Soares Marmorato (CICBEU)', '2008-04-23', '09:00:00', '10:45:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-02 15:48:52', '143.107.227.80'),
(6, 'ABERTURA', '<p></p>', 'Profa Dra Maria do Carmo Calijuri (Diretora da EESC)', '2008-04-22', '09:00:00', '09:10:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-02 11:45:21', '143.107.227.80'),
(7, 'TREINAMENTO: Portal Periódicos CAPES - Base de Dados', '<ul><li>Apresenta&ccedil;&atilde;o do Portal de Peri&oacute;dicos da Capes</li></ul>\r\n<p class="MsoNormal">&nbsp;</p><ul><li>Web of Science: Science Citation Index; Social Sciences Citation Index; Arts &amp; Humanities Citation &Iacute;ndex (Thomson)</li></ul>\r\n<p style="padding-left: 60px;">Base de dados<span> </span>multidisciplinar de resumos que indexa os peri&oacute;dicos mais citados em suas respectivas &aacute;reas e possui hoje mais de 9.000 peri&oacute;dicos indexados. &Eacute; tamb&eacute;m um &iacute;ndice de cita&ccedil;&otilde;es, informando, para cada artigo, os documentos por ele citados e os documentos que o citaram.</p>\r\n<p class="MsoNormal">&nbsp;</p><ul><li>Scopus (Elsevier)</li></ul>\r\n<p class="MsoNormal" style="padding-left: 60px;">Base de dados de resumos e de cita&ccedil;&otilde;es da literatura cient&iacute;fica e de fontes de informa&ccedil;&atilde;o de n&iacute;vel acad&ecirc;mico na Internet. Indexa mais de 15 mil peri&oacute;dicos, cerca de 265 milh&otilde;es p&aacute;ginas da Internet, 18 milh&otilde;es de patentes, al&eacute;m de outros documentos. Cobre as &aacute;reas<span> </span>de Ci&ecirc;ncias Biol&oacute;gicas, Ci&ecirc;ncias da Sa&uacute;de, Ci&ecirc;ncias F&iacute;sicas e Ci&ecirc;ncias Sociais.</p><ul><li>Compendex (Elsevier)</li></ul>\r\n<p class="MsoNormal" style="padding-left: 60px;">Base de dados de resumos com mais de 7 milh&otilde;es de registros indexados nas &aacute;reas de Engenharia Civil, Energia, Engenharia Ambiental, Engenharia Qu&iacute;mica, Engenharia de Minas, Engenharia Metal&uacute;rgica, Engenharia T&eacute;rmica, Engenharia Mec&acirc;nica, Engenharia Aeroespacial, Engenharia Nuclear, Engenharia de Transportes, Engenharia Naval, Ci&ecirc;ncia da Computa&ccedil;&atilde;o, Rob&oacute;tica e Controle. Indexa mais de 5.000 publica&ccedil;&otilde;es peri&oacute;dicas, trabalhos de congressos e confer&ecirc;ncias, livros e relat&oacute;rios governamentais.</p>', 'Tutilla de Brito Aragão (CAPES) e Adriana Rodrigues (Elsevier)', '2008-04-22', '09:00:00', '17:30:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-22 15:44:39', '143.107.227.80'),
(9, 'MINI-CURSO: Fator de Impacto e Índice H', '<p>Descreve como &eacute; calculado o Fator de Impacto, o &Iacute;ndice H e outros &iacute;ndices que avaliam a qualidade de um peri&oacute;dico cient&iacute;fico baseado na freq&uuml;&ecirc;ncia de cita&ccedil;&otilde;es.</p>', 'Rosana Alvarez Paschoalino (Bibliotecária EESC)', '2008-04-22', '19:00:00', '22:00:00', 100, '1', 'Sala de aula da Biblioteca', 2008, '2008-04-22 11:53:40', '143.107.227.80'),
(10, 'PALESTRA: Conduta e Equilíbrio Emocional', '<p></p>\r\n<p>Como as exig&ecirc;ncias do mundo moderno podem influenciar na op&ccedil;&atilde;o pela p&oacute;s-gradua&ccedil;&atilde;o e a import&acirc;ncia da Psicologia neste paradigma.</p>', 'Ana Cristina Roma (Psicóloga)', '2008-04-23', '11:00:00', '12:00:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-02 11:42:29', '143.107.227.80'),
(11, 'MESA REDONDA: Como publicar um Artigo Científico', '<p>Apresenta&ccedil;&atilde;o das experi&ecirc;ncias relacionadas ao processo de submiss&atilde;o e avalia&ccedil;&atilde;o de trabalhos destinados &agrave; publica&ccedil;&atilde;o em revistas cient&iacute;ficas. Aborda, tamb&eacute;m, o processo de cria&ccedil;&atilde;o de uma revista cient&iacute;fica</p>', 'Professores Doutores Elias Hage Junior (UFSCar/ABPol), José Reynaldo A. Setti (STT), Luiz Gonçalves Neto (SEL), Marcio Minto Fabrício (SAP)', '2008-04-23', '14:00:00', '17:00:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-22 15:59:40', '143.107.227.80'),
(12, 'PALESTRA: Trabalho Científico: Preparo e Apresentação', '<ul><li>Caracter&iacute;ticas b&aacute;sicas, etapas e qualidades da \r\ninvestiga&ccedil;&atilde;o cient&iacute;fica</li><li>O que um projeto deve conter</li><li>Plano do \r\ntrabalho cient&iacute;fico</li><li>Compromisso &eacute;tico e normativo</li></ul>', 'Profa Dra Victória Secaf (EE/USP)', '2008-04-24', '14:00:00', '17:00:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-02 11:43:48', '143.107.227.80'),
(13, 'PALESTRA: Slides para Apresentações Eficientes', '<p>&nbsp;</p>\r\n<p>Para destacar-se atualmente, &eacute; preciso demonstrar o conhecimento da forma mais \r\neficiente poss&iacute;vel e as apresenta&ccedil;&otilde;es s&atilde;o oportunidades de sucesso. Nesta \r\noportunidade, diversas ferramentas e dicas ser&atilde;o apresentadas para enriquecer as \r\nsuas apresenta&ccedil;&otilde;es.</p>', 'Fagner França (Doutorando em Geotecnia – EESC/USP)', '2008-04-25', '09:00:00', '11:00:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-02 10:25:37', '143.107.227.80'),
(14, 'PALESTRA: Biblioteca Digital de Teses e Dissertações', '<p></p>\r\n<p>Apresenta&ccedil;&atilde;o da BDTD/USP e demonstra&ccedil;&atilde;o do processo de\r\nsubmiss&atilde;o. Apresenta&ccedil;&atilde;o de outras bibliotecas digitais de teses e disserta&ccedil;&otilde;es\r\ndo Brasil e do Mundo, com acesso gratuito ao texto completo</p>', 'Teresinha das Graças Coletta (Diretora Técnica SVBIBL/EESC)', '2008-04-25', '11:15:00', '12:15:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-02 10:39:48', '143.107.227.80'),
(15, 'PALESTRA/DEBATE: A Pós-Graduação na USP e sua Internacionalização', '<p>Apresenta&ccedil;&atilde;o, seguida de debate, sobre a p&oacute;s-gradua&ccedil;&atilde;o na USP. O debate ser&aacute; conduzido pelo Prof. Geraldo R. Martins da Costa (Presidente da CPG/EESC/USP) e pela Profa. Maria do Carmo Calijuri (Diretora da EESC), contando com a participa&ccedil;&atilde;o de coordenadores de programas de p&oacute;s, professores e alunos<span> </span>da EESC, das demais Unidades USP e demais interessados.</p>', 'Prof Dr Armando Corbani (Pró-Reitor de Pós Graduação da USP)', '2008-04-25', '14:00:00', '17:00:00', 150, '1', 'Auditório do CETEPE', 2008, '2008-04-15 14:18:32', '143.107.227.80'),
(16, 'ENCERRAMENTO', '<p>&nbsp;</p>', '.', '2008-04-25', '17:00:00', '18:00:00', 50, '1', 'Biblioteca', 2008, '2008-04-02 15:37:56', '143.107.227.110'),
(17, 'VIVÊNCIA: Técnicas de Relaxamento para Pós-Graduandos', '<p>&nbsp;</p>\r\n<p>Durante a viv&ecirc;ncia ser&atilde;o demonstradas t&eacute;cnicas diferentes de relaxamento para obten&ccedil;&atilde;o de uma maior consci&ecirc;ncia corporal e, conseq&uuml;entemente, diminui&ccedil;&atilde;o de stress e melhor rendimento acad&ecirc;mico.</p>', 'Ligia Mochida (Fisioterapeuta – mestranda da UFSCar)', '2008-04-24', '19:00:00', '21:00:00', 100, '1', 'Sala de aula da Biblioteca', 2008, '2008-04-22 11:43:02', '143.107.227.80');
