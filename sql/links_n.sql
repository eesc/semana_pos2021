-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 05:07 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_links_n`
--

CREATE TABLE IF NOT EXISTS `semanapos_links_n` (
  `infos` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ano`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_links_n`
--

INSERT INTO `semanapos_links_n` (`infos`, `ano`, `ip`, `data_hora`) VALUES
('\r\n<p style="text-align: center;">&nbsp;<span style="font-size: small; font-weight: bold;">Transmiss&atilde;o ao vivo</span></p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: x-small;"><span style="font-size: small;">Algumas palestras ser&atilde;o transmitidas ao vivo pelo <a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="http://iptv.usp.br/portal/scheduled-transmissions.action" target="_blank">IPTV - USP</a> e pelo&nbsp;<a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="https://www.youtube.com/channel/UCMyis-JuG1rD2eCXNLQq0Ag">Canal Youtube da Biblioteca</a></span></span></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><strong><span style="font-size: x-small;"><span style="font-size: small;">Aplicativo para Celular</span></span></strong></span></p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: small;"><span style="color: #222222; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;">Acompanhe a programa&ccedil;&atilde;o pelo aplicativo&nbsp;</span><a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="http://free.eventbase.com/eventbase/index.php" target="_blank">Eventbase</a>.&nbsp;</span></span></p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: small;">Baixe no<span style="color: #222222; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;">&nbsp;Play Store, App Store ou no site: (</span><a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="http://free.eventbase.com/eventbase/index.php" target="_blank">http://free.eventbase.com/eventbase/index.php</a><span style="color: #222222; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;">.)</span></span></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><a href="http://free.eventbase.com/eventbase/index.php" target="_blank"><img style="vertical-align: middle;" src="http://biblioteca.eesc.usp.br/images/event.png" alt="" width="135" height="135" /></a>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<a href="http://free.eventbase.com/eventbase/index.php" target="_blank"><img style="vertical-align: middle;" src="http://biblioteca.eesc.usp.br/images/code.png" alt="" width="165" height="165" /></a>&nbsp;</p>\r\n<h2 style="text-align: center;"><a style="color: #2a6496; box-sizing: border-box; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-origin: initial; background-clip: initial; background-color: transparent; outline: -webkit-focus-ring-color auto 5px;" title="Introdu&ccedil;&atilde;o &agrave; P&oacute;s-Gradua&ccedil;&atilde;o da Universidade de S&atilde;o Paulo" href="http://www.prpg.usp.br/index.php/pt-br/noticias/3610-introducao-a-pos-graduacao-da-universidade-de-sao-paulo" target="_blank">Introdu&ccedil;&atilde;o &agrave; P&oacute;s-Gradua&ccedil;&atilde;o da Universidade de S&atilde;o Paulo</a></h2>\r\n<p style="text-align: center;"><a href="https://edisciplinas.usp.br/course/index.php?categoryid=2520"><img src="http://eesc.usp.br/eesc/administracao/biblioteca/pub/semana_pos2017/images/ediscip.jpg" alt="" /></a></p>\r\n', 2017, '143.107.182.88', '2017-03-07 10:43:44'),
('\r\n<p style="text-align: center;">&nbsp;<span style="font-size: small; font-weight: bold;">Transmiss&atilde;o ao vivo</span></p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: x-small;"><span style="font-size: small;">Algumas palestras ser&atilde;o transmitidas ao vivo pelo <a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="http://iptv.usp.br/portal/scheduled-transmissions.action" target="_blank">IPTV - USP</a> e pelo&nbsp;<a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="https://www.youtube.com/channel/UCMyis-JuG1rD2eCXNLQq0Ag">Canal Youtube da Biblioteca</a></span></span></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><strong><span style="font-size: x-small;"><span style="font-size: small;">Acompanhe nossa agenda </span></span></strong></span></p>\r\n<iframe src="https://calendar.google.com/calendar/embed?mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=usp.br_h0nu1bqib7rlmpv078cgb89bt4%40group.calendar.google.com&amp;color=%23875509&amp;ctz=America%2FSao_Paulo" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>', 2019, '143.107.182.88', '2018-02-15 00:00:00'),
('\r\n<p style="text-align: center;">&nbsp;<span style="font-size: small; font-weight: bold;">Transmiss&atilde;o ao vivo</span></p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: x-small;"><span style="font-size: small;">Algumas palestras ser&atilde;o transmitidas ao vivo pelo <a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="http://iptv.usp.br/portal/scheduled-transmissions.action" target="_blank">IPTV - USP</a> e pelo&nbsp;<a style="color: #1155cc; font-family: arial, sans-serif; font-size: 12.8px; text-align: start;" href="https://www.youtube.com/channel/UCMyis-JuG1rD2eCXNLQq0Ag">Canal Youtube da Biblioteca</a></span></span></span></p>\r\n<p style="text-align: center;">&nbsp;</p>\r\n<p style="text-align: center;"><span style="font-size: x-small;"><strong><span style="font-size: x-small;"><span style="font-size: small;">Acompanhe nossa agenda </span></span></strong></span></p>\r\n<iframe src="https://calendar.google.com/calendar/embed?mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=usp.br_h0nu1bqib7rlmpv078cgb89bt4%40group.calendar.google.com&amp;color=%23875509&amp;ctz=America%2FSao_Paulo" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>', 2020, '143.107.182.88', '2020-09-30 00:00:00');
