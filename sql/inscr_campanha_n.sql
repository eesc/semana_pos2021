-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 05:05 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_inscr_campanha_n`
--

CREATE TABLE IF NOT EXISTS `semanapos_inscr_campanha_n` (
  `pagina` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ano`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_inscr_campanha_n`
--

INSERT INTO `semanapos_inscr_campanha_n` (`pagina`, `ano`, `ip`, `data_hora`) VALUES
('<p><span style="font-family: ">Durante a XV\r\nSemana da P&oacute;s-gradua&ccedil;&atilde;o na EESC de 2019 estamos arrecadando </span><span style="font-family: Verdana, sans-serif;">produtos de higiene pessoal (desodorante spray, xampu, barbeador, fralda tamanho G e EG),&nbsp;</span><span style="font-family: Verdana, sans-serif;">sacos de lixos (200L), luvas, pano de limpeza leve descart&aacute;vel, &aacute;lcool,&nbsp;</span><span style="font-family: Verdana, sans-serif;">desinfetante e &aacute;gua sanit&aacute;ria</span><span style="font-family: "> para&nbsp;</span><span style="font-family: Verdana, sans-serif;">doa&ccedil;&atilde;o ao&nbsp;"Cantinho Fraterno D. Maria Jacinta e ao Abrigo de Idosos Dona Helena Dornfeld.</span></p>\r\n<p><a href="https://www.santacasasaocarlos.com.br/BancoDeSangue" target="_blank">Doa&ccedil;&atilde;o de Sangue - Banco de Sangue da Santa Casa de Miseric&oacute;rdia de S&atilde;o Carlos.</a></p>', 2019, '143.107.227.79', '2019-01-29 09:30:17');
