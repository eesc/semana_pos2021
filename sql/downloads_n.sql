-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 05:00 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_downloads_n`
--

CREATE TABLE IF NOT EXISTS `semanapos_downloads_n` (
  `id_download` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `fisico` text NOT NULL,
  `tamanho` int(11) NOT NULL,
  `descricao` text NOT NULL,
  `ano` int(4) NOT NULL,
  `data_hora` datetime NOT NULL,
  `ip` text NOT NULL,
  PRIMARY KEY (`id_download`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Extraindo dados da tabela `semanapos_downloads_n`
--

INSERT INTO `semanapos_downloads_n` (`id_download`, `titulo`, `fisico`, `tamanho`, `descricao`, `ano`, `data_hora`, `ip`) VALUES
(3, 'Como a produ&ccedil;&atilde;o cient&iacute;fica afeta as rela&ccedil;&otilde;es humanas na Universidade', 'Produtivismo.pdf', 355907, '', 2014, '2014-04-24 07:45:31', '143.107.227.120'),
(4, 'Processo editorial das publica&ccedil;&otilde;es cient&iacute;ficas da Thomson Reuters', 'indexingthomsonreuters.pdf', 1786729, '', 2014, '2014-04-24 16:20:34', '143.107.227.120'),
(10, 'Comunica&ccedil;&atilde;o como ferramenta chave na pesquisa', 'Material_BibliotecaEESC_EugeniaDajer_2014 (1).pdf', 3306471, '', 2014, '2014-05-20 15:48:26', '143.107.227.110'),
(8, 'Google Drive, OneDrive ou DropBox: roda de conversa sobre armazenamento em nuvem (parte 1)', 'sev1.flv', 133000, '', 2014, '2014-05-08 15:53:03', '143.107.227.120\r\n'),
(11, 'Apresenta&ccedil;&atilde;o de produtos e lan&ccedil;amento do trial da base de e-books AccessEngineering, da Editora McGraw-Hill', 'EESC USP Abril 2015 v2.ppsx', 1814692, '', 2015, '2015-05-18 15:03:38', '143.107.227.110'),
(9, 'Google Drive, OneDrive ou DropBox: roda de conversa sobre armazenamento em nuvem (parte 2)', 'sev2.flv', 133000, '', 2014, '2014-05-08 15:55:52', '143.107.227.120'),
(21, 'A p&oacute;s-gradua&ccedil;&atilde;o na USP', '2016a.pdf', 952231, '', 2016, '2016-04-28 16:09:42', '143.107.227.110'),
(14, 'Palestra &quot;Escrita cient&iacute;fica&quot;', 'Biblio-eesc-2015.ppt', 690176, '', 2015, '2015-05-27 12:08:07', '143.107.227.96'),
(15, 'Treinamento End Note Web – gerenciamento de referencias bibliogr&aacute;ficas', 'Gerenciadores de referencia.pdf', 3919671, '', 2015, '2015-05-27 12:11:39', '143.107.227.96'),
(17, 'Treinamento &quot;Bases de Dados de Patentes&quot;', 'Apresentacao_Daniel_29_04_EESC.pdf', 2892308, '', 2015, '2015-05-28 10:03:28', '143.107.227.96'),
(22, 'Como publicar um artigo cient&iacute;fico dicas de editores e revisores  - Marco Henriq', '2016b.pdf', 607535, '', 2016, '2016-04-28 16:11:26', '143.107.227.110'),
(19, 'Mesa redonda &quot;Suporte Institucional&quot;- Portal da Escrita Cient&iacute;fica', 'escrita_cientifica3.pptx', 10365355, '', 2015, '2015-05-28 17:49:55', '143.107.227.96'),
(23, 'Escrita cient&iacute;fica em ingl&ecirc;s', '2016c.pdf', 3201478, '', 2016, '2016-04-28 16:12:43', '143.107.227.110'),
(24, 'Portal da Escrita Cient&iacute;fica do Campus USP S&atilde;o Carlos', '2016d.pdf', 4681293, '', 2016, '2016-04-28 16:13:34', '143.107.227.110'),
(25, 'Qualis - arq1', '2016e.pdf', 415271, '', 2016, '2016-04-28 16:14:26', '143.107.227.110'),
(26, 'Qualis - arq2', '2016f.pdf', 452529, '', 2016, '2016-04-28 16:14:49', '143.107.227.110'),
(27, 'Qualis - arq3', '2016g.pdf', 510022, '', 2016, '2016-04-28 16:15:12', '143.107.227.110'),
(28, 'Regimentos e regulamentos da p&oacute;s-gradua&ccedil;&atilde;o na USP e na EESC', '2016h.pdf', 4704640, '', 2016, '2016-04-28 16:15:35', '143.107.227.110'),
(29, '&Eacute;tica em pesquisa na EESC', '2016i.pdf', 434547, '', 2016, '2016-04-28 16:19:37', '143.107.227.110'),
(30, 'Apresentando-se bem com o Curr&iacute;culo Lattes', '2016j.pdf', 2632934, '', 2016, '2016-04-29 07:51:54', '143.107.227.110'),
(31, 'PBL - problem based learning', '2016k.pdf', 1740421, '', 2016, '2016-05-02 11:30:18', '143.107.227.110'),
(32, 'Bases de dados - Acesso aos conte&uacute;dos da CAPES, Access Engineering, Ebooks em Engenharia e Engineering case studies', '2016l.pdf', 507800, '', 2016, '2016-05-02 11:36:15', '143.107.227.110'),
(33, 'EndNote Basic: gerenciador de refer&ecirc;ncias', '2016m.pdf', 5298710, '', 2016, '2016-05-04 14:45:14', '143.107.227.110'),
(34, 'Relat&oacute;rio Semana da P&oacute;s-gradua&ccedil;&atilde;o 2016', 'Relatorio_Semana_da_pos_graduacao_2016.pdf', 299907, '', 2016, '2016-06-02 11:12:20', '143.107.227.85'),
(40, 'WorkShop ORCID para autores', 'orcid.pdf', 12117781, '', 2017, '2017-03-17 12:29:46', '143.107.227.79'),
(42, 'Minicurso Apresentando-se bem com o Curr&iacute;culo Lattes', 'lattes.pdf', 3180280, '', 2017, '2017-03-17 12:28:33', '143.107.227.79'),
(43, 'Mesa redonda Egressos da EESC: Mercado e Academia', 'mercado-academia.pdf', 1693841, '', 2017, '2017-03-17 16:09:10', '143.107.227.79'),
(44, 'Palestra Detectando pl&aacute;gio em manuscritos', 'plagio.pdf', 3989061, '', 2017, '2017-03-17 12:38:13', '143.107.227.79'),
(45, 'IEEE workshop para autores', 'ieee.pdf', 6105008, '', 2017, '2017-03-17 12:38:59', '143.107.227.79'),
(46, 'Treinamento Escrita Cient&iacute;fica ', 'artigos.pdf', 583739, '', 2017, '2017-03-17 16:08:29', '143.107.227.79'),
(47, 'Treinamento Access Engineering', 'bases.pdf', 481239, '', 2017, '2017-03-17 16:09:02', '143.107.227.79'),
(48, 'Minicurso Estrutura de trabalho acad&ecirc;mico, cita&ccedil;&otilde;es e refer&ecirc;ncias e demais normas aceitas pela USP para disserta&ccedil;&otilde;es e teses', 'estrutura.pdf', 363498, '', 2017, '2017-03-17 16:49:27', '143.107.227.79'),
(49, 'Minicurso EndNote Basic: gerenciador de refer&ecirc;ncias', 'gerenciadores.pdf', 4775930, '', 2017, '2017-03-17 16:49:58', '143.107.227.79'),
(50, 'Palestra Acesso aberto e a visibilidade dos peri&oacute;dicos brasileiros', 'periodicos.pdf', 2167323, '', 2017, '2017-03-21 08:37:01', '143.107.227.79'),
(51, 'Mesa redonda &quot;Capta&ccedil;&atilde;o de recursos&quot;', 'captacao.pdf', 376852, '', 2017, '2017-03-22 11:32:24', '143.107.227.79'),
(52, 'Palestra &quot;Qualis&quot;', 'qualis.pdf', 1593862, '', 2017, '2017-03-22 11:32:58', '143.107.227.79'),
(53, 'Webinar &quot;Mendeley&quot;', 'mendeley.pdf', 4664204, '', 2017, '2017-03-24 08:01:50', '143.107.227.79'),
(55, 'Palestra “Apresentando-se bem com o Curr&iacute;culo Lattes&quot;', 'XIV Semana Lattes.pdf', 497319, '', 2018, '2018-03-16 07:45:07', '143.107.227.79'),
(56, 'Palestra “ORCiD para pesquisadores&quot;', 'XIV Semana ORCiD.pdf', 792592, '', 2018, '2018-03-16 07:46:31', '143.107.227.79'),
(57, 'Palestra “Soul Conscious Access – uma estrat&eacute;gia para desenvolver a sa&uacute;de da mente e o equil&iacute;brio das emo&ccedil;&otilde;es”', 'XIV Semana Soul.pdf', 8235814, '', 2018, '2018-03-16 07:53:54', '143.107.227.79'),
(58, 'Ciclo de palestras “Informa&ccedil;&otilde;es gerais, oportunidades e conviv&ecirc;ncia na USP – S&atilde;o Carlos”', 'XIV Semana Ciclo bib.pdf', 2012596, 'Elenise Maria de Araujo (EESC – Biblioteca)', 2018, '2018-03-16 08:02:19', '143.107.227.79'),
(59, 'Ciclo de palestras “Informa&ccedil;&otilde;es gerais, oportunidades e conviv&ecirc;ncia na USP – S&atilde;o Carlos”', 'XIV Semana Ciclo pos.pdf', 952743, 'Cesar Roberto Derisso (EESC - Servi&ccedil;o de P&oacute;s-gradua&ccedil;&atilde;o)', 2018, '2018-03-16 08:03:13', '143.107.227.79'),
(60, 'Palestra “Mendeley – gerenciador de refer&ecirc;ncia”', 'XIV Semana Mendeley.pdf', 3553401, '', 2018, '2018-03-20 11:49:21', '143.107.227.79'),
(61, 'Palestra “Estrutura de trabalho acad&ecirc;mico, cita&ccedil;&otilde;es e refer&ecirc;ncias e demais normas aceitas pela USP para disserta&ccedil;&otilde;es e teses”', 'XIV Semana Estrutura.pdf', 361133, '', 2018, '2018-03-20 11:50:07', '143.107.227.79'),
(62, 'Mesa redonda &quot;Internacionaliza&ccedil;&atilde;o da P&oacute;s-Gradua&ccedil;&atilde;o: Oportunidades de conv&ecirc;nios e acordos de coopera&ccedil;&atilde;o internacional para pesquisa&quot;', 'XIV Semana Internacionalizacao Luis.pdf', 1099750, 'Prof. Luis Fernando Costa Alberto (EESC - SEL / Presidente da Comiss&atilde;o de P&oacute;s-Gr', 2018, '2018-03-20 16:47:14', '143.107.227.79'),
(63, 'Mesa redonda &quot;Internacionaliza&ccedil;&atilde;o da P&oacute;s-Gradua&ccedil;&atilde;o: Oportunidades de conv&ecirc;nios e acordos de coopera&ccedil;&atilde;o internacional para pesquisa&quot;', 'XIV Semana Internacionalizacao Denis.pdf', 2451367, 'Prof. Denis Vinicius Coury (CCINT/SEL/EESC)', 2018, '2018-03-20 16:47:47', '143.107.227.79'),
(64, 'Palestra &quot;Artigos Cient&iacute;ficos – Dicas e Sugest&otilde;es&quot;', 'XIV Semana Artigos.pdf', 1223346, '', 2018, '2018-03-20 16:53:57', '143.107.227.79'),
(67, 'Mesa redonda “Inova&ccedil;&atilde;o e patentes” (i) ', 'arquivo5a.pdf', 6130229, '', 2019, '2019-03-22 16:53:43', '143.107.227.79'),
(68, 'Mesa redonda “Inova&ccedil;&atilde;o e patentes” (ii)', 'arquivo5b.pdf', 2423161, '', 2019, '2019-03-22 16:39:30', '143.107.227.79'),
(69, 'Ciclo de palestras “Informa&ccedil;&otilde;es gerais, oportunidades e conviv&ecirc;ncia na USP – S&atilde;o Carlos” (i)', 'arquivo7a.pdf', 2190242, '', 2019, '2019-03-22 16:45:47', '143.107.227.79'),
(70, 'Ciclo de palestras “Informa&ccedil;&otilde;es gerais, oportunidades e conviv&ecirc;ncia na USP – S&atilde;o Carlos” (ii)', 'arquivo7b.pdf', 731936, '', 2019, '2019-03-22 16:46:01', '143.107.227.79'),
(71, 'Mesa redonda “Boas pr&aacute;ticas na pesquisa” (i)', 'arquivo9a.pdf', 1051143, '', 2019, '2019-03-22 16:46:50', '143.107.227.79'),
(72, 'Mesa redonda “Boas pr&aacute;ticas na pesquisa” (ii)', 'arquivo9b.pdf', 2801687, '', 2019, '2019-03-22 16:47:03', '143.107.227.79'),
(73, 'Workshop “Base de dados Elsevier para engenharia”', 'arquivo12.pdf', 2348158, '', 2019, '2019-03-22 16:48:14', '143.107.227.79'),
(74, 'Palestra “Template em Latex”', 'arquivo17.pdf', 3120679, '', 2019, '2019-03-22 16:48:50', '143.107.227.79'),
(75, 'Palestra &quot;Escreva sem medo: t&eacute;cnicas para enfrentar a p&aacute;gina em branco&quot;', 'arquivo18.pdf', 1814826, '', 2019, '2019-03-22 16:49:43', '143.107.227.79'),
(76, 'Workshop “Web of Science, JCR e Endnote Web”', 'arquivo20.pdf', 324658, '', 2019, '2019-03-22 16:50:18', '143.107.227.79'),
(77, 'Palestra “Artigos cient&iacute;ficos - dicas e sugest&otilde;es”', 'arquivo26.pdf', 1289964, '', 2019, '2019-03-22 16:50:55', '143.107.227.79'),
(78, 'Palestra “Gest&atilde;o do tempo de pesquisa”', 'arquivo28.pdf', 3731763, '', 2019, '2019-03-22 16:51:26', '143.107.227.79'),
(79, 'Workshop “Sage research methods e os conceitos b&aacute;sicos em metodologia e pesquisa', 'arquivo31.pdf', 817041, '', 2019, '2019-03-25 11:46:47', '143.107.227.79'),
(80, 'Palestra “Orcid para pesquisadores”', 'arquivo22.pdf', 813345, '', 2019, '2019-03-25 07:30:27', '143.107.227.79');
