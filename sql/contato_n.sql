-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 04:54 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_contato_n`
--

CREATE TABLE IF NOT EXISTS `semanapos_contato_n` (
  `contato` text NOT NULL,
  `ano` int(4) NOT NULL DEFAULT '0',
  `ip` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `texto` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `semanapos_contato_n`
--

INSERT INTO `semanapos_contato_n` (`contato`, `ano`, `ip`, `data_hora`, `texto`) VALUES
('semana@eesc.usp.br', 2009, '143.107.182.78', '2009-04-14 15:58:42', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da V Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno.</p>\r\n<p style="text-align: center;">&nbsp;</p>'),
('semana@eesc.usp.br', 2010, '143.107.227.80', '2010-04-12 16:54:24', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da VI Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno.</p>'),
('bibeesc@sc.usp.br', 2011, '143.107.227.85', '2011-04-14 10:55:22', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da VII Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno.</p>'),
('bibeesc@sc.usp.br', 2012, '143.107.227.110', '2012-04-05 16:54:48', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da VIII Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno.</p>'),
('bibeesc@sc.usp.br', 2013, '143.107.227.110', '2013-04-05 16:54:48', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da IX Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno.</p>'),
('bibeesc@sc.usp.br', 2014, '143.107.227.110', '2014-04-02 16:41:18', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da X Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno</p>'),
('biblioteca@eesc.usp.br', 2016, '143.107.182.88', '2016-03-21 17:42:00', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da XII Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno</p>'),
('biblioteca@eesc.usp.br', 2017, '143.107.227.79', '2017-01-27 11:29:28', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da <strong>XIII Semana da P&oacute;s-Gradua&ccedil;&atilde;o na EESC</strong>, preencha os campos abaixo que em breve receber&aacute; retorno</p>'),
('biblioteca@eesc.usp.br', 2015, '143.107.182.88', '2015-01-05 00:00:00', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da XI Semana "A P&oacute;s-Gradua&ccedil;&atilde;o da EESC na Biblioteca", preencha os campos abaixo que em breve receber&aacute; retorno</p>'),
('semana@eesc.usp.br', 2018, '143.107.227.79', '2018-11-28 15:58:22', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da <strong>XV Semana da P&oacute;s-Gradua&ccedil;&atilde;o na EESC</strong>, preencha os campos abaixo que em breve receber&aacute; retorno</p>'),
('semana@eesc.usp.br', 2020, '143.107.182.88', '0000-00-00 00:00:00', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da <strong>XVI Semana da P&oacute;s-Gradua&ccedil;&atilde;o na EESC</strong>, preencha os campos abaixo que em breve receber&aacute; retorno</p>'),
('semana@eesc.usp.br', 2019, '143.107.182.88', '0000-00-00 00:00:00', '<p style="padding-left: 30px;">Para entrar em contato com a organiza&ccedil;&atilde;o da <strong>XV Semana da P&oacute;s-Gradua&ccedil;&atilde;o na EESC</strong>, preencha os campos abaixo que em breve receber&aacute; retorno</p>');
