-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2019 as 05:10 PM
-- Versão do Servidor: 5.1.73
-- Versão do PHP: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `eesc_biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `semanapos_prog_n2020`
--

CREATE TABLE IF NOT EXISTS `semanapos_prog_n2020` (
  `id_prog` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `resumo` text NOT NULL,
  `palestrante` text NOT NULL,
  `data` date NOT NULL DEFAULT '0000-00-00',
  `horainicio` time NOT NULL DEFAULT '00:00:00',
  `horafim` time NOT NULL DEFAULT '00:00:00',
  `nrovagas` smallint(6) NOT NULL DEFAULT '0',
  `nrovagasVC` smallint(6) NOT NULL,
  `disponivel` char(1) NOT NULL DEFAULT '1',
  `local` text NOT NULL,
  `data_hora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_prog`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Extraindo dados da tabela `semanapos_prog_n2020`
--

INSERT INTO `semanapos_prog_n2020` (`id_prog`, `titulo`, `resumo`, `palestrante`, `data`, `horainicio`, `horafim`, `nrovagas`, `nrovagasVC`, `disponivel`, `local`, `data_hora`, `ip`) VALUES
(4, 'Atividades de interação entre os alunos nos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-11', '08:00:00', '18:00:00', 500, 1000, '0', 'Departamentos da EESC', '2019-01-21 11:02:10', '143.107.227.79'),
(3, 'Abertura', '<p>&nbsp;</p>', 'Prof. Carlos Gilberto Carlotti Junior (USP - Pró-Reitor de Pós-Graduação)<br>Prof. Edson Cezar Wendland (EESC - Diretor)<br>Prof. Murilo Araujo Romero (EESC - Presidente da Comissão de Pós-graduação)<br>Prof. Carlos Alberto Fortulan (EESC - Presidente da Comissão de Biblioteca)', '2019-03-12', '08:45:00', '09:15:00', 30, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-22 16:16:05', '143.107.227.79'),
(5, 'Mesa redonda “Inovação e patentes” ', '<p>&nbsp;</p>', 'Prof. Vanderlei Salvador Bagnato (IFSC - Diretor)<br>Prof. Leandro Innocentini L. de Faria (UFSCAR - NIT)<br><br>Mediador<br>Prof. Daniel Varela Magalhães (EESCin - SEM)', '2019-03-12', '09:15:00', '10:30:00', 109, 1000, '1', 'Anfiteatro “Jorge Caron”', '2019-03-11 15:41:56', '143.107.227.79'),
(6, 'Abertura exposição de livros dos docentes da EESC, pôsteres e estandes dos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-12', '10:30:00', '11:00:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-18 11:47:42', '143.107.227.79'),
(7, 'Ciclo de palestras “Informações gerais, oportunidades e convivência na USP – São Carlos”', '<p>&nbsp;CANCELADO</p>', 'Cesar Roberto Derisso (EESC - SVPOS)<br>Elenise Maria de Araújo (EESC – Biblioteca)<br>Lea S.M. Gonçalves (EESC - STI)<br>Elio Tarpani Junior (PUSP – DVCOMUN)<br>Maria Cecilia H. T. Cavalheiro (PUSP - LRQ)<br>Daniel Garcia Ribeiro (Associação dos pós-graduandos da USP-São Carlos)', '2019-03-12', '11:00:00', '12:15:00', 99, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-21 15:36:10', '143.107.227.79'),
(8, 'Apresentação Gaperia - bateria do CAASO', '<p>&nbsp;</p>', '.', '2019-03-12', '12:15:00', '12:30:00', 500, 1000, '0', 'Piso térreo - Bloco E-1', '2019-01-18 11:49:21', '143.107.227.79'),
(9, 'Mesa redonda “Boas práticas na pesquisa”', '<p>&nbsp;</p>', 'Prof. Hamilton B. Varela de Albuquerque (IQSC - Vice-diretor)<br>Prof. Tercio Ambrizzi (IAG - USP)<br><br>Mediador<br>Prof. Eduardo Mario Mendiondo (EESC - SHS)', '2019-03-12', '14:00:00', '15:30:00', 131, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-11 15:44:03', '143.107.227.79'),
(10, 'Exposição de livros dos docentes da EESC, pôsteres e estandes dos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-12', '15:30:00', '16:00:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-11 16:12:26', '143.107.227.79'),
(11, 'Palestra “Recursos para detectar plágio em manuscritos”', '<p>&nbsp;</p>', 'Profa. Luciana Montanari (EESC - SEM)', '2019-03-12', '16:00:00', '16:30:00', 33, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-11 15:44:38', '143.107.227.79'),
(12, 'Workshop “Base de dados Elsevier para engenharia”', '<p>&nbsp;</p>', 'Luiz Baginski / Sérgio Vidal  (Elsevier)', '2019-03-12', '16:30:00', '17:30:00', 100, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-11 15:45:27', '143.107.227.79'),
(14, 'Mesa redonda “Relação entre universidade e setor privado: a Pós-Graduação e suas oportunidades”', '<p>&nbsp;</p>', 'Fernando Fernandez (Materials & Processes R&D Engineer EMBRAER)<br>Prof. José Alberto Cuminato (Coordenador CEPI/CeMEAI, ICMC-USP)<br><br>Mediador<br>Prof. Fernando M. Catalano (SAA - EESC/USP)', '2019-03-13', '09:00:00', '10:15:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-21 15:37:24', '143.107.227.79'),
(15, 'Exposição de livros dos docentes da EESC, pôsteres e estandes dos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-13', '10:15:00', '10:45:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-18 11:51:24', '143.107.227.79'),
(16, 'Palestra “Normas e estrutura de trabalho acadêmico para dissertações e teses da USP”', '<p>&nbsp;</p>', 'Elena Luzia Palloni Gonçalves (Bibliotecária EESC/USP)', '2019-03-13', '10:45:00', '11:15:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:30:00', '143.107.227.79'),
(17, 'Palestra “Template em Latex”', '<p>&nbsp;</p>', 'Ana Paula A. Calabrez e<br>Marilza Tognetti Ap. Rodrigues (Bibliotecárias PUSP São Carlos) ', '2019-03-13', '11:15:00', '12:15:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:30:11', '143.107.227.79'),
(18, 'Palestra "Escreva sem medo: técnicas para enfrentar a página em branco"', '<p>&nbsp;</p>', 'Denise Casatti (ICMC - Assessoria de Comunicação)', '2019-03-13', '14:00:00', '16:00:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-26 11:01:07', '143.107.227.79'),
(19, 'Exposição de livros dos docentes da EESC, pôsteres e estandes dos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-13', '16:00:00', '16:30:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-15 16:12:18', '143.107.227.79'),
(20, 'Workshop “Web of Science, JCR e Endnote Web”', '<p>&nbsp;</p>', 'Deborah Dias (Clarivate Analytics)', '2019-03-13', '16:30:00', '18:00:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-01 15:40:18', '143.107.227.79'),
(45, 'Credenciamento', '<p>&nbsp;</p>', '.', '2019-03-12', '08:00:00', '09:00:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-03-01 15:37:51', '143.107.227.79'),
(22, 'Palestra “Orcid para pesquisadores”', '<p>&nbsp;</p>', 'Eduardo Graziosi Silva (Bibliotecário EESC/USP)', '2019-03-13', '18:00:00', '18:30:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:30:43', '143.107.227.79'),
(23, 'Palestra “Bem estar emocional”', '<p>&nbsp;</p>', 'Bárbara Kolstok Monteiro (IFSC Psicóloga)', '2019-03-14', '09:00:00', '09:45:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:30:57', '143.107.227.79'),
(24, 'Palestra “Como falar em público”', '<p>&nbsp;</p>', 'Amelie Cintra (Psicóloga Clínica e Consultora de Recursos Humanos)', '2019-03-14', '09:45:00', '10:30:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:31:10', '143.107.227.79'),
(25, 'Exposição de livros dos docentes da EESC, pôsteres e estandes dos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-14', '10:30:00', '11:00:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-16 10:43:54', '143.107.227.79'),
(26, 'Palestra “Artigos científicos - dicas e sugestões”', '<p>&nbsp;</p>', 'Marcos Criado (Dot Lib)', '2019-03-14', '11:00:00', '12:00:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:31:22', '143.107.227.79'),
(27, 'Apresentação do coral USP', '<p>&nbsp;</p>', '.', '2019-03-14', '13:00:00', '13:45:00', 500, 1000, '0', 'Piso térreo - Bloco E-1', '2019-01-18 11:53:43', '143.107.227.79'),
(28, 'Palestra “Gestão do tempo de pesquisa”', '<p>&nbsp;</p>', 'Profa. Eva Lantsoght (Univ. Quito-Equador) - videoconferência em inglês', '2019-03-14', '14:00:00', '14:45:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-02-20 11:31:43', '143.107.227.79'),
(29, 'Palestra “Gestão do tempo”', '<p>&nbsp;CANCELADO</p>', 'Felipe Guedes Pinheiro (Mosaico 21 - Desenvolvimento Humano)', '2019-03-14', '14:45:00', '15:30:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-21 15:36:33', '143.107.227.79'),
(30, 'Exposição de livros dos docentes da EESC, pôsteres e estandes dos programas de Pós-Graduação', '<p>&nbsp;</p>', '.', '2019-03-14', '15:30:00', '16:00:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-18 11:55:28', '143.107.227.79'),
(31, 'Workshop  “Sage research methods e os conceitos básicos em  metodologia e pesquisa”', '<p>&nbsp;</p>', 'Bruno Machado (Sage Pub)', '2019-03-14', '16:00:00', '18:00:00', 500, 1000, '1', 'Anfiteatro Jorge Caron', '2019-03-25 11:46:04', '143.107.227.79'),
(32, 'Aula de Zouk - grupo do  CAASO', '<p>&nbsp;</p>', '.', '2019-03-14', '18:15:00', '19:00:00', 500, 1000, '0', 'Espaço Primavera - piso térreo bloco E-1', '2019-01-18 11:56:29', '143.107.227.79'),
(34, 'Plantão Dúvidas Sobre Gerenciadores de Referências (Mendeley e Endnote Web)', '<p>&nbsp;</p>', '.', '2019-03-15', '14:00:00', '17:00:00', 500, 1000, '0', 'Sala de pesquisa da Biblioteca da EESC/USP', '2019-03-13 11:25:19', '143.107.227.79'),
(35, 'Atividades de interação entre os alunos nos programas de Pós-Graduação Palestrante	.', '<p>&nbsp;</p>', '.', '2019-03-15', '08:00:00', '18:00:00', 500, 1000, '0', 'Departamentos da EESC', '2019-01-16 10:42:13', '143.107.227.79'),
(37, 'Encerramento', '<p>&nbsp;</p>', 'Profa. Ana Paula Camargo Larocca (EESC - SET)<br>Elenise Maria de Araujo (Biblioteca - Coordenação da Semana)', '2019-03-14', '18:00:00', '18:15:00', 25, 1000, '0', 'Anfiteatro Jorge Caron', '2019-03-21 15:40:43', '143.107.227.79'),
(38, 'Treinamento Solid Edge', '<p><a href="http://www.biblioteca.eesc.usp.br/images/semana/treinamento.pdf" target="_blank">Resumo</a></p>', 'Empresa SmartPLM', '2019-03-15', '08:30:00', '12:30:00', 87, 1000, '1', 'STI/EESC/USP (Sala GRAD01 - Piso térreo) ', '2019-03-13 11:25:30', '143.107.227.79'),
(40, 'Plantão Dúvidas Sobre Gerenciadores de Referências (Mendeley e Endnote Web)', '<p>&nbsp;</p>', '.', '2019-03-15', '08:00:00', '11:00:00', 500, 1000, '0', 'Sala de pesquisa da Biblioteca da EESC/USP', '2019-02-20 11:32:41', '143.107.227.79'),
(42, 'Avaliação Física', '<p><span style="white-space: pre;"> </span></p>', '.', '2019-03-15', '08:30:00', '09:30:00', 500, 1000, '0', 'Cefer', '2019-01-30 16:34:05', '143.107.227.79'),
(43, 'Jogo Recreativo de Queimada', '<p><span style="white-space: pre;"> </span></p>', '.', '2019-03-15', '10:00:00', '12:00:00', 500, 1000, '0', 'Cefer', '2019-01-30 16:35:16', '143.107.227.79');
